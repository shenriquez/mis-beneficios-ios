//
//  TestViewController.swift
//  NFTopMenuController
//
//  Created by Niklas Fahl on 12/16/14.
//  Copyright (c) 2014 Niklas Fahl. All rights reserved.
//

import UIKit

class HomeFavoritosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lbCantidad: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var titleLabel: UILabel!

    var arrayOfertasFavorito = Array<Oferta>()
    var searchActive : Bool = false
    var filtered = Array<Oferta>()
    var oneTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayOfertasFavorito = VariablesGlobales.perfil.favoritos
        
        tableView.registerNib(UINib(nibName: "CustomCellDescuento", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellDescuentos")
    
        tableView.estimatedRowHeight = 60.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = true
        
        let gesture = UILongPressGestureRecognizer.init(target: self, action: #selector(self.eliminarFavorito(_:)))
        tableView.addGestureRecognizer(gesture)
        
        
        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        arrayOfertasFavorito = VariablesGlobales.perfil.favoritos
        tableView.reloadData()
        
        let indexPath = self.tableView.indexPathForSelectedRow;
        if ((indexPath) != nil) {
            tableView.deselectRowAtIndexPath(indexPath!, animated: animated)
        }
        if(VariablesGlobales.refresPerfil)
        {
            VariablesGlobales.refresPerfil = false
            self.performSelectorInBackground(#selector(self.getPerfil), withObject: nil)
            
        }

    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        arrayOfertasFavorito = VariablesGlobales.perfil.favoritos
        tableView.reloadData()

    }
    
    
    func getPerfil(){
        Request().getPerfil(VariablesGlobales.perfil.email)
        arrayOfertasFavorito = VariablesGlobales.perfil.favoritos
        tableView.reloadData()
        BaseHomeViewController.container.removeFromSuperview()
    }

    
      
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(arrayOfertasFavorito.count == 0)
        {
            return 1
        }
        
        if(searchActive) {
            lbCantidad.text = String(filtered.count);
            return filtered.count
        }
        lbCantidad.text = String(arrayOfertasFavorito.count);
        return arrayOfertasFavorito.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellDescuentos", forIndexPath: indexPath) as! CustomCellHomeTableViewCell
        
        if(arrayOfertasFavorito.count == 0)
        {
            cell.imgFavorito.hidden = true
            cell.lbTitulo.text = "No has guardado ningún beneficio como favorito"
            cell.lbCuenta.text = ""
            cell.lbCategoria.text = ""
            cell.imgCategoria.image = UIImage.init(named: "alerta")
            cell.userInteractionEnabled = false
        
        }
        else
        {
            cell.imgFavorito.hidden = false
             cell.userInteractionEnabled = true
            if(searchActive){
                
                cell.lbTitulo.text = filtered[indexPath.row].titulo! + " " + filtered[indexPath.row].descuento!
                cell.lbCuenta.text = filtered[indexPath.row].nombreConvenio + " / "
                cell.lbCategoria.text = filtered[indexPath.row].categoria + " - " + filtered[indexPath.row].subCategoria
                cell.imgCategoria.image = UIImage.init(named: filtered[indexPath.row].fotoCategoria)
                
            } else {
                
                cell.lbTitulo.text = arrayOfertasFavorito[indexPath.row].titulo! + " " + arrayOfertasFavorito[indexPath.row].descuento!
                cell.lbCuenta.text = arrayOfertasFavorito[indexPath.row].nombreConvenio + " / "
                cell.lbCategoria.text = arrayOfertasFavorito[indexPath.row].categoria + " - " + arrayOfertasFavorito[indexPath.row].subCategoria
                cell.imgCategoria.image = UIImage.init(named: arrayOfertasFavorito[indexPath.row].fotoCategoria)
                
            }
                    
        }
        
        return cell
    }

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
       return true
    }
    
    
    func eliminarFavorito(ges: UILongPressGestureRecognizer)  {
        
        let touch = ges.locationInView(self.tableView)
        let alertController = UIAlertController(title: "", message: "¿Quieres eliminar este beneficio de tus favoritos?", preferredStyle: .ActionSheet)
        
        let defaultAction = UIAlertAction(title: "Eliminar", style: .Destructive, handler: { (alert: UIAlertAction)-> Void in            
            let indexPath = self.tableView.indexPathForRowAtPoint(touch)
             Request().eliminarFavorito(VariablesGlobales.perfil.email, id_descuento: self.arrayOfertasFavorito[indexPath!.row].idOferta)
             self.arrayOfertasFavorito.removeAtIndex(indexPath!.row)
            VariablesGlobales.perfil.favoritos = self.arrayOfertasFavorito
            VariablesGlobales.perfil.numFavoritos = self.arrayOfertasFavorito.count
            VariablesGlobales.arrayOfertas = Request().obtenerOfertas(VariablesGlobales.perfil.gustos, convenios: VariablesGlobales.perfil.convenios, email: VariablesGlobales.perfil.email)

            if(self.arrayOfertasFavorito.count == 0)
            {
                while (self.tableView.gestureRecognizers!.count != 0) {
                    self.tableView.removeGestureRecognizer(self.tableView.gestureRecognizers![0])
                }
            }
            self.tableView.reloadData()
            self.lbCantidad.text = String(self.arrayOfertasFavorito.count);
            
        })
        alertController.addAction(defaultAction)
        let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)

      }

    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = false;
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        self.view.endEditing(true)
        searchBar.showsCancelButton = false
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var oferta: Oferta
        if(searchActive){
            
            oferta = filtered[indexPath.row]
        } else {
            oferta = arrayOfertasFavorito[indexPath.row]
          
        }
        
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleOfertaViewController = storyboard.instantiateViewControllerWithIdentifier("detalleOferta") as! DetalleOfertaViewController
        
        vc.oferta = oferta
        vc.arrayOfertas = arrayOfertasFavorito
        
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchActive = true;
        self.tableView.reloadData()
        searchBar.showsCancelButton = false
       
    }

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = arrayOfertasFavorito.filter({ $0.nombreConvenio.lowercaseString.containsString(searchText) || $0.nombreConvenio.containsString(searchText) || $0.titulo!.lowercaseString.containsString(searchText) || $0.titulo!.containsString(searchText) || $0.categoria.lowercaseString.containsString(searchText) || $0.categoria.containsString(searchText) || $0.subCategoria.lowercaseString.containsString(searchText) || $0.subCategoria.containsString(searchText) })
        
        if(searchText == ""){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
    
   
}
