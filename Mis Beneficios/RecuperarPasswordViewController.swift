//
//  RecuperarPasswordViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 04-07-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class RecuperarPasswordViewController: UIViewController {

    @IBOutlet weak var txtCorreo: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPressed(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func continuePresses(sender: UIButton){
        
        if validateTextMail(){
            if checkEmail(txtCorreo.text!){
                let alertController = UIAlertController(title: "", message: "Debe llamar al WS!", preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                presentViewController(alertController, animated: true, completion: nil)
            }else{
                let alertController = UIAlertController(title: "", message: "Por favor ingresa tu correo de manera correcta", preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                presentViewController(alertController, animated: true, completion: nil)
            }
        }else{
            let alertController = UIAlertController(title: "", message: "Por favor ingresa tu Correo", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
        
        
        
    }
    
    func checkEmail(email: String) -> Bool {
        var result = false
        var atSeparetedStrings = email.componentsSeparatedByString("@")
        let atSeparetedStringsCount = atSeparetedStrings.count
        
        if atSeparetedStringsCount == 2 {
            let firstString = atSeparetedStrings[0]
            
            if(firstString.characters.count > 0 )
            {
                let otherString = atSeparetedStrings[1]
                let dotRange = otherString.rangeOfString(".")
                if(dotRange != nil)
                {
                    let dotLocation = otherString.startIndex.distanceTo(dotRange!.startIndex)
                    
                    if(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < (otherString.characters.count - 1)))
                    {
                        result = true
                    }
                }
                
            }
        }
        
        return result
    }
    
    func validateTextMail() -> Bool{
        var result = false
        
        if txtCorreo.text?.characters.count > 0 {
            result = true
        }
        
        return result
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
