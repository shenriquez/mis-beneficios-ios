//
//  dateComboBox.h
//  BancaMobilIphone
//
//  Created by Medianet on 19/11/14.
//  Copyright (c) 2014 com.provincial.bancamovil2. All rights reserved.
//
#import "dateComboBoxDelegate.h"
#import <UIKit/UIKit.h>
#import "Constantes_Macros.h"

@interface dateComboBox : UIDatePicker<UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    UITextField * _textField;
    NSString * _fechaMuestra;
    NSString * _fechaEnvio;
    id <dateComboBoxDelegate> _delegado;
    BOOL taped;
}
@property (nonatomic,strong) UITextField * textField;
@property (nonatomic,strong) NSString * fechaEnvio;
@property (nonatomic,strong) NSString * fechaMuestra;
@property (nonatomic,strong) id <dateComboBoxDelegate> delegado;

-(void)serasVistaDeEntradaParaUITextField:(UITextField*)t;
@end

