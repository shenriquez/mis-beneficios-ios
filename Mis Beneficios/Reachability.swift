//
//  Reachability.swift
//  Mis Beneficios
//
//  Created by Sarai on 04-07-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

public class Reachability {
    
    class func isConnectedToNetwork()->Bool{
        
        var Status:Bool = false
        let url = NSURL(string: "http://google.com/")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "HEAD"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10.0
        let session = NSURLSession.sharedSession()
        
        session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            
            if let httpResponse = response as? NSHTTPURLResponse {
                print("httpResponse.statusCode \(httpResponse.statusCode)")
                if httpResponse.statusCode == 200 {
                    Status = true
                }else{
                    let alertController = UIAlertController(title: "", message: "Sin conexión de Internet", preferredStyle: .Alert)

                    let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    let alertWindow = UIWindow(frame: UIScreen.mainScreen().bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
                }
            }
            
        }).resume()
        
        
        return Status
    }
}
