//
//  BuscadorViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 12-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class BuscadorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    var filtered = Array<Oferta>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "CustomCellDescuento", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellDescuentos")
        
        tableView.estimatedRowHeight = 60.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = true
        tableView.separatorStyle = .None
        // Do any additional setup after loading the view.
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let indexPath = self.tableView.indexPathForSelectedRow;
        if ((indexPath) != nil) {
            tableView.deselectRowAtIndexPath(indexPath!, animated: animated)
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if(searchActive)
        {
            if(filtered.count == 0)
            {
                return 1
            }
        }
        return filtered.count

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellDescuentos", forIndexPath: indexPath) as! CustomCellHomeTableViewCell
        
        if(filtered.count == 0)
        {
            cell.lbTitulo.text = "No se han encontrado beneficios que coincidan con tu busqueda"
            cell.lbCuenta.text = ""
            cell.lbCategoria.text = ""
            cell.imgCategoria.image = UIImage.init(named: "alerta")
            cell.userInteractionEnabled = false
            cell.imgFavorito.hidden = true
        }
        else
        {
            cell.userInteractionEnabled = true
            cell.lbTitulo.text = filtered[indexPath.row].titulo! + " " + filtered[indexPath.row].descuento!
            cell.lbCuenta.text = filtered[indexPath.row].nombreConvenio + " / "
            cell.lbCategoria.text = filtered[indexPath.row].categoria + " - " + filtered[indexPath.row].subCategoria

            cell.imgCategoria.image = UIImage.init(named: filtered[indexPath.row].fotoCategoria)
            
            if(filtered[indexPath.row].favorito == 0)
            {
                cell.imgFavorito.hidden = false
            }
            else
            {
                cell.imgFavorito.hidden = true
            }

        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = false;
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        self.view.endEditing(true)
        searchBar.showsCancelButton = false
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let oferta = filtered[indexPath.row]
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleOfertaViewController = storyboard.instantiateViewControllerWithIdentifier("detalleOferta") as! DetalleOfertaViewController
        
        vc.oferta = oferta
        
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchActive = true;
        self.tableView.reloadData()
        searchBar.showsCancelButton = false
        
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
      filtered = VariablesGlobales.arrayOfertas.filter({ $0.nombreConvenio.lowercaseString.containsString(searchText) || $0.nombreConvenio.containsString(searchText) || $0.titulo!.lowercaseString.containsString(searchText) || $0.titulo!.containsString(searchText) || $0.categoria.lowercaseString.containsString(searchText) || $0.categoria.containsString(searchText) || $0.subCategoria.lowercaseString.containsString(searchText) || $0.subCategoria.containsString(searchText) })
  
        self.tableView.reloadData()
    }
}
