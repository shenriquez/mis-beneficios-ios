//
//  AppDelegate.swift
//  Mis Beneficios
//
//  Created by Ramon on 02-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Google
import FBSDKCoreKit
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let userDefaults = NSUserDefaults.standardUserDefaults()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
      
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        print(userDefaults.boolForKey("secondTime"))
        
        if(self.userDefaults.boolForKey("secondTime"))
        {
    
            self.window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
           // self.window?.backgroundColor = UIColor.whiteColor()
            self.window?.makeKeyAndVisible()
            var navController: UINavigationController!
            var storyboard: UIStoryboard
            
            storyboard = UIStoryboard(name: "Intro", bundle: nil)
            navController = UINavigationController.init(rootViewController: storyboard.instantiateViewControllerWithIdentifier("login") as! IniciarSesionViewController)
            navController.navigationBarHidden = true;
            self.window?.rootViewController = navController
        }
       
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        
        return true
    }
    
    func application(application: UIApplication,
                     openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handleURL(url,
                                                    sourceApplication: sourceApplication,
                                                    annotation: annotation)
        
        let facebookDidHandle =  FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        
         return googleDidHandle || facebookDidHandle
    
    }
    
    
   
    
    @available(iOS 9.0, *)
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {

        let googleDidHandle = GIDSignIn.sharedInstance().handleURL(url,
                                                                   sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as! String?,
                                                                   annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
        
        let facebookDidHandle =  FBSDKApplicationDelegate.sharedInstance().application(app, openURL: url, sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as! String?, annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
        
        return googleDidHandle || facebookDidHandle
    }
    
    
   

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
               
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
          UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
          UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }
    

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("DEVICE TOKEN = \(deviceToken)")
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        let json = JSON(userInfo)
        print(json)
        
        
        
    }
}

