//
//  TabBarController.swift
//  Mis Beneficios
//
//  Created by Sarai on 06-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TabBarController: UITabBarController {

    let userDefaults = NSUserDefaults.standardUserDefaults()
    var cuentas = ""
    var gustosGuardados:[Int] = []
    var cuentasGuardadas:[Int] = []
    internal static var container: UIView = UIView()
    var goTo = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userDefaults.setBool(true, forKey: "secondTime")
        self.userDefaults.synchronize()

        
        if(VariablesGlobales.arrayOfertas.count == 0){
            self.refreshOfertas()
        }
        
        if(goTo != 0 )
        {
            self.selectedIndex = goTo

        }
    }
    
    func refreshOfertas(){
        
        VariablesGlobales.arrayOfertas = Request().obtenerOfertas(VariablesGlobales.perfil.gustos, convenios: VariablesGlobales.perfil.convenios, email: VariablesGlobales.perfil.email)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
       

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if(item.tag == 3){
            if (VariablesGlobales.refresPerfil) {
                performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            }
        }
        
    }

    
    func showActivityIndicatory(uiView: UIView) {
        TabBarController.container.frame = uiView.frame
        TabBarController.container.center = uiView.center
        TabBarController.container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        TabBarController.container.addSubview(loadingView)
        uiView.addSubview(TabBarController.container)
        actInd.startAnimating()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
