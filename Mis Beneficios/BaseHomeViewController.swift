//
//  BaseHomeViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 10-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import PageMenu


class BaseHomeViewController: UIViewController {
    
    
    @IBOutlet weak var viewPageMenu: UIView!
    var pageMenu : CAPSPageMenu?
    internal static var goTo = 0
    var container: UIView = UIView()
    internal static var container: UIView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        if(VariablesGlobales.refresPerfil)
        {
            Request().getPerfil(VariablesGlobales.perfil.email)
            VariablesGlobales.refresPerfil = false
        }
        
        let controller2 : HomeFavoritosViewController = HomeFavoritosViewController ()
        controller2.title = "favoritos"
        controllerArray.append(controller2)
        
        let controller3 : BeneficiosCompartidosViewController = BeneficiosCompartidosViewController ()
        controller3.title = "compartidos"
        controllerArray.append(controller3)
        
        let controller4 : BeneficiosGuardadosViewController = BeneficiosGuardadosViewController ()
        controller4.title = "calendario"
        controllerArray.append(controller4)

        
        // Customize menu (Optional)
        var parameters: [CAPSPageMenuOption] = [
            .ScrollMenuBackgroundColor(UIColor.whiteColor()),
            .ViewBackgroundColor(UIColor.clearColor()),
            .SelectionIndicatorColor(COLOR_RED),
            .BottomMenuHairlineColor(UIColor.clearColor()),
            .MenuItemFont(UIFont(name: "Signika-Light", size: 13.0)!),
            .MenuHeight(30.0),
            .CenterMenuItems(true)
        ]
        
        if(self.view.frame.size.width <= 375){
            parameters.append(CAPSPageMenuOption.MenuItemWidth(90.0))
        }
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, viewPageMenu.frame.width, viewPageMenu.frame.height), pageMenuOptions: parameters)
        
        pageMenu?.selectedMenuItemLabelColor = COLOR_RED
        pageMenu?.moveToPage(1)
        pageMenu?.moveToPage(BaseHomeViewController.goTo)
        BaseHomeViewController.goTo = 0
        
        TabBarController.container.removeFromSuperview();
  
        self.addChildViewController(pageMenu!)
        viewPageMenu.addSubview(pageMenu!.view)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        // MARK: - Scroll menu setup
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        BaseHomeViewController.goTo = 0
        if(VariablesGlobales.refresPerfil)
        {
            Request().getPerfil(VariablesGlobales.perfil.email)
            VariablesGlobales.refresPerfil = false
        }
    }
    
    func showActivityIndicatory(uiView: UIView) {
        BaseHomeViewController.container.frame = uiView.frame
        BaseHomeViewController.container.center = uiView.center
        BaseHomeViewController.container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        BaseHomeViewController.container.addSubview(loadingView)
        uiView.addSubview(BaseHomeViewController.container)
        actInd.startAnimating()
    }

    
    
    // MARK: - Container View Controller
    override func shouldAutomaticallyForwardAppearanceMethods() -> Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }

    
}
