//
//  SeleccionContentViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 22-06-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class SeleccionContentViewController: UIViewController {
    
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var btMasTarde: UIButton!
    @IBOutlet weak var btBack: UIButton!
    @IBOutlet var tableView: UITableView!
    var array = Array<Array<String?>>()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var prefersBancos:[Int] = []
    var prefersTarjetas:[Int] = []
    var prefersTelefonia:[Int] = []
    var prefersLectores:[Int] = []
    var conveniosGuardados:[Int] = []
    var pageController: SeleccionPageController!
    var order: [UIViewController]!
    var titulo = String()
    var pageIndex: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        lbTitulo.text = titulo
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        if(VariablesGlobales.convenios.count == 0)
        {
            VariablesGlobales.convenios = Request().getConvenios()
        }
        
        if VariablesGlobales.convenios.count != 0 {
            
            for index in 0...VariablesGlobales.convenios.count-1 {
                if((VariablesGlobales.convenios[index].dictionaryValue["tipo"]!.string) == "1"){
                    
                    let banco = [VariablesGlobales.convenios[index].dictionaryValue["nombre_convenio"]!.string, VariablesGlobales.convenios[index].dictionaryValue["id_convenio"]!.string]
                    
                    array.append(banco)
                }
            }
        }
        
        if (VariablesGlobales.perfil.convenios != "")
        {
            var myStringArr = VariablesGlobales.perfil.convenios.componentsSeparatedByString(",")
            for index in 0...myStringArr.count-1 {
                conveniosGuardados.append(Int(myStringArr[index])!)
            }
        }
        
        if(conveniosGuardados.count != 0)
        {
            for index in 0...conveniosGuardados.count-1 {
                for index1 in 0...VariablesGlobales.convenios.count-1 {
                    if((VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue) == conveniosGuardados[index]){
                        
                        switch VariablesGlobales.convenios[index1].dictionaryValue["tipo"]!.intValue {
                        case 1:
                            prefersBancos.append(VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue)
                        case 2:
                            prefersTarjetas.append(VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue)
                        case 3:
                            prefersTelefonia.append(VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue)
                        case 4:
                            prefersLectores.append(VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue)
                        default:
                            prefersLectores.append(VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue)
                        }
                        break
                    }
                }
                
            }
        }
        
        userDefaults.setObject(prefersBancos, forKey: "prefersBanco")
        userDefaults.synchronize()
        
        userDefaults.setObject(prefersTarjetas, forKey: "prefersTarjetas")
        userDefaults.synchronize()
        
        userDefaults.setObject(prefersTelefonia, forKey: "prefersTelefonia")
        userDefaults.synchronize()
        
        userDefaults.setObject(prefersLectores, forKey: "prefersLectores")
        userDefaults.synchronize()
        
        
        if(userDefaults.boolForKey("secondTime"))
        {
            btMasTarde.hidden = true
            btBack.hidden = false
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        var vc1 = UIStoryboard(name: "Intro", bundle: nil) .
            instantiateViewControllerWithIdentifier("seleccion_bancos") as! SeleccionBancosViewController
        
        return [vc1,
                UIStoryboard(name: "Intro", bundle: nil) .
                    instantiateViewControllerWithIdentifier("seleccion_tarjetas"),
                UIStoryboard(name: "Intro", bundle: nil) .
                    instantiateViewControllerWithIdentifier("seleccion_telefonia"),
                UIStoryboard(name: "Intro", bundle: nil) .
                    instantiateViewControllerWithIdentifier("seleccion_otros")]
        
    }()
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("celdaBancos", forIndexPath: indexPath) as! CustomCell
        var banco = array[indexPath.row]
        cell.txtContenido.text = banco[0]
        cell.uiSwitch.tag = indexPath.row
        if(prefersBancos.count > 0){
            let id = banco[1]
            for index in 0...prefersBancos.count-1 {
                if Int(id!) == prefersBancos[index] {
                    cell.uiSwitch.on = true
                    cell.txtContenido.textColor = COLOR_BLUE
                    cell.txtContenido.font = UIFont.systemFontOfSize(14, weight: UIFontWeightBold)
                    break
                }
            }
        }
        
        return cell
        
    }
    
    
    @IBAction func chanceValue(sender: UISwitch) {
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! CustomCell;
        if sender.on {
            cell.txtContenido.textColor = COLOR_BLUE
            cell.txtContenido.font = UIFont.systemFontOfSize(14, weight: UIFontWeightBold)
            var banco = array[sender.tag]
            let id = banco[1]
            prefersBancos.append(Int(id!)!)
            print(prefersBancos)
        }
        else{
            cell.txtContenido.textColor = UIColor.init(red:CGFloat(70/255.0), green: CGFloat(70/255.0), blue: CGFloat(70/255.0), alpha: 1.0)
            cell.txtContenido.font = UIFont.systemFontOfSize(14, weight: UIFontWeightLight)
            var banco = array[sender.tag]
            let id = banco[1]
            if(prefersBancos.count > 0){
                for index in 0...prefersBancos.count-1 {
                    if Int(id!)! == prefersBancos[index] {
                        prefersBancos.removeAtIndex(index)
                        print(prefersBancos)
                        return
                    }
                }
            }
            
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "continuarTarjetas"
        {
            if segue.destinationViewController is SeleccionTarjetasViewController {
            }
        }
    }
    
    
    @IBAction func back(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func savePrefers(sender: AnyObject) {
        
    }
    
}
