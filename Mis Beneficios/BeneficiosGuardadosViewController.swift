//
//  BeneficiosGuardadosViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 5/26/16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class BeneficiosGuardadosViewController: UITableViewController {
    
    var arrayGuardados = Array<Oferta>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayGuardados = VariablesGlobales.perfil.recordados
     
        self.tableView.registerNib(UINib(nibName: "CustomCellDescuento", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellDescuentos")
        
        tableView.estimatedRowHeight = 60.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = true
        tableView.separatorStyle = .None
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        arrayGuardados = VariablesGlobales.perfil.recordados
        tableView.reloadData()
        let indexPath = self.tableView.indexPathForSelectedRow;
        if ((indexPath) != nil) {
            tableView.deselectRowAtIndexPath(indexPath!, animated: animated)
        }
        if(VariablesGlobales.refresPerfil)
        {
            VariablesGlobales.refresPerfil = false
            self.performSelectorInBackground(#selector(self.getPerfil), withObject: nil)
            
        }
    }
   
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        arrayGuardados = VariablesGlobales.perfil.recordados
        tableView.reloadData()
    }
    
    func getPerfil(){
        Request().getPerfil(VariablesGlobales.perfil.email)
        arrayGuardados = VariablesGlobales.perfil.recordados
        tableView.reloadData()
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(arrayGuardados.count == 0)
        {
            return 1
        }
        return arrayGuardados.count;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellDescuentos", forIndexPath: indexPath) as! CustomCellHomeTableViewCell
        
        if(arrayGuardados.count == 0)
        {
            cell.lbTitulo.text = "No has guardado ningún beneficio en tu calendario"
            cell.lbCuenta.text = ""
            cell.lbCategoria.text = ""
            cell.imgCategoria.image = UIImage.init(named: "alerta")
            cell.userInteractionEnabled = false
            cell.imgFavorito.hidden = true
            
        }
        else
        {
            cell.lbTitulo.text = arrayGuardados[indexPath.row].titulo! + " " + arrayGuardados[indexPath.row].descuento!
            cell.lbCuenta.text = arrayGuardados[indexPath.row].nombreConvenio + " / "
            cell.lbCategoria.text = arrayGuardados[indexPath.row].categoria + " - " + arrayGuardados[indexPath.row].subCategoria
            cell.imgCategoria.image = UIImage.init(named:arrayGuardados[indexPath.row].fotoCategoria)
            
            if(arrayGuardados[indexPath.row].favorito == 0)
            {
                cell.imgFavorito.hidden = false
            }
            else
            {
                cell.imgFavorito.hidden = true
            }

            
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleOfertaViewController = storyboard.instantiateViewControllerWithIdentifier("detalleOferta") as! DetalleOfertaViewController
        let oferta = arrayGuardados[indexPath.row]
        vc.oferta = oferta
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(false)
      
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}