//
//  DetalleMisBeneficiosViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 13-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import EventKit

class DetalleMisBeneficiosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbNumOfertas: UILabel!
    @IBOutlet weak var lbCategoria: UILabel!
    @IBOutlet weak var imgCategoria: UIImageView!
    var categoria: Categoria!
    @IBOutlet weak var tableView: UITableView!
    var container: UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbTitulo.text = categoria.nombreCategoria
        lbCategoria.text = categoria.nombreCategoria
        lbNumOfertas.text = String(categoria.cantidadOfertas)
        imgCategoria.image = categoria.imgCategoria
        
        
        self.tableView.registerNib(UINib(nibName: "CustomCellDescuento", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellDescuentos")

        
        tableView.allowsSelection = true
        tableView.separatorStyle = .None
        


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        let indexPath = self.tableView.indexPathForSelectedRow;
        if ((indexPath) != nil) {
            tableView.deselectRowAtIndexPath(indexPath!, animated: animated)
        }
        self.tableView.reloadData()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoria.ofertas.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellDescuentos", forIndexPath: indexPath) as! CustomCellHomeTableViewCell
        
        cell.lbTitulo.text = categoria.ofertas[indexPath.row].titulo! + " " + categoria.ofertas[indexPath.row].descuento!
        cell.lbCuenta.text = categoria.ofertas[indexPath.row].nombreConvenio + " / "
        cell.lbCategoria.text = categoria.ofertas[indexPath.row].categoria + " - " +  categoria.ofertas[indexPath.row].subCategoria
        cell.imgCategoria.image = UIImage.init(named: categoria.ofertas[indexPath.row].fotoCategoria)
        
        if(categoria.ofertas[indexPath.row].favorito == 0)
        {
            cell.imgFavorito.hidden = false
        }
        else
        {
            cell.imgFavorito.hidden = true
        }
        
        return cell

    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func  tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
       // let favorito = UITableViewRowAction(style: .Default, title: "♡\n favorito") { action, index in
        let favorito = UITableViewRowAction(style: .Default, title: "favorito") { action, index in
            VariablesGlobales.refresPerfil = true
           
            self.performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            let response = Request().agregarFavorito(VariablesGlobales.perfil.email, id_descuento: self.categoria.ofertas[indexPath.row].idOferta)
            VariablesGlobales.arrayOfertas = Request().obtenerOfertas(VariablesGlobales.perfil.gustos, convenios: VariablesGlobales.perfil.convenios, email: VariablesGlobales.perfil.email)
            self.container.removeFromSuperview()            
            if(response)
            {
                let alertController = UIAlertController(title: "", message: "Este beneficio se ha agregado a tus favoritos exitosamente", preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                self.categoria.ofertas[indexPath.row].favorito = 0
                self.presentViewController(alertController, animated: true, completion: nil)
                self.tableView.reloadData()
            }
            else
            {
                let alertController = UIAlertController(title: "", message: "Este beneficio ya se encuentra agregado a tus favoritos", preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
            }
            self.tableView(tableView, commitEditingStyle: UITableViewCellEditingStyle.Delete, forRowAtIndexPath: indexPath)
        }

        
       // let guardar = UITableViewRowAction(style: .Default, title: "\u{1F4C5}\n guardar") { action, index in
        let guardar = UITableViewRowAction(style: .Default, title: "guardar") { action, index in
           self.addEventToCalendar(title: self.categoria.ofertas[indexPath.row].titulo!, description: self.categoria.ofertas[indexPath.row].descripcion!, startDate: NSDate(), endDate: NSDate(), idOferta: self.categoria.ofertas[indexPath.row].idOferta)
            self.tableView(tableView, commitEditingStyle: UITableViewCellEditingStyle.Insert, forRowAtIndexPath: indexPath)
        }
        guardar.backgroundColor = UIColor.orangeColor()
        

        
        return [ guardar, favorito]
    }
    
        
    func addEventToCalendar(title title: String, description: String?, startDate: NSDate, endDate: NSDate, idOferta: Int, completion: ((success: Bool, error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        
        eventStore.requestAccessToEntityType(.Event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.saveEvent(event, span: .ThisEvent)
                    Request().recordar(VariablesGlobales.perfil.email, id_descuento: idOferta)
                } catch let e as NSError {
                    completion?(success: false, error: e)
                    return
                }
                completion?(success: true, error: nil)
            } else {
                completion?(success: false, error: error)
            }
            
            
        })
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleOfertaViewController = storyboard.instantiateViewControllerWithIdentifier("detalleOferta") as! DetalleOfertaViewController
        let oferta = categoria.ofertas[indexPath.row]
        vc.oferta = oferta
        vc.arrayOfertas = categoria.ofertas
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    

    @IBAction func back(sender: AnyObject) {
        
           self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showActivityIndicatory(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }

}
