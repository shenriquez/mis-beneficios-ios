//
//  SeleccionPageController.swift
//  Mis Beneficios
//
//  Created by Sarai on 22-06-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class SeleccionPageController: UIPageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .Forward,
                               animated: true,
                               completion: nil)
        }
    }
    
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
       var vc1 = UIStoryboard(name: "Intro", bundle: nil) .
            instantiateViewControllerWithIdentifier("seleccion_bancos") as! SeleccionBancosViewController
        vc1.pageController = self
        
        
        return [vc1,
                UIStoryboard(name: "Intro", bundle: nil) .
                    instantiateViewControllerWithIdentifier("seleccion_tarjetas"),
                UIStoryboard(name: "Intro", bundle: nil) .
                    instantiateViewControllerWithIdentifier("seleccion_telefonia"),
            UIStoryboard(name: "Intro", bundle: nil) .
                        instantiateViewControllerWithIdentifier("seleccion_otros")]
        
    }()

}

// MARK: UIPageViewControllerDataSource

extension SeleccionPageController: UIPageViewControllerDataSource {
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.indexOf(viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.indexOf(viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }

}
