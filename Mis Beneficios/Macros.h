//
//  Macros.h
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <Foundation/Foundation.h>

//WS PRODUCCION
#define WS_ADDRESS_BOOK @"https://backend.eatnip.com/mobile/addressbook.php"
#define WS_ADDTOCART @"https://backend.eatnip.com/mobile/addtocart.php"
#define WS_CUSTOMER_ORDER @"https://backend.eatnip.com/mobile/customerorder.php"
#define WS_CHECKOUT @"https://backend.eatnip.com/mobile/checkout.php"
#define WS_LOGIN @"https://backend.eatnip.com/mobile/login.php"
#define WS_SIGNIN @"https://backend.eatnip.com/mobile/register.php"
#define WS_MENU_LIST @"https://backend.eatnip.com/mobile/menuList.php"
#define WS_PROFILE @"https://backend.eatnip.com/mobile/profileAddEdit.php"
#define WS_REGISTER @"https://backend.eatnip.com/mobile/mobile/register.php"
#define WS_REVIEWS @"https://backend.eatnip.com/mobile/restaurantReviews.php"

//WS DESARROLLO
//#define WS_ADDRESS_BOOK @"http://nipdev.mobilemediacms.com/mobile/addressbook.php"
//#define WS_ADDTOCART @"http://nipdev.mobilemediacms.com/mobile/addtocart.php"
//#define WS_CUSTOMER_ORDER @"http://nipdev.mobilemediacms.com/mobile/customerorder.php"
//#define WS_CHECKOUT @"http://nipdev.mobilemediacms.com/mobile/checkout.php"
//#define WS_LOGIN @"http://nipdev.mobilemediacms.com/mobile/login.php"
//#define WS_SIGNIN @"http://nipdev.mobilemediacms.com/mobile/register.php"
//#define WS_MENU_LIST @"http://nipdev.mobilemediacms.com/mobile/menuList.php"
//#define WS_PROFILE @"http://nipdev.mobilemediacms.com/mobile/profileAddEdit.php"
//#define WS_REGISTER @"http://nipdev.mobilemediacms.com/mobile/mobile/register.php"
//#define WS_REVIEWS @"http://nipdev.mobilemediacms.com/mobile/restaurantReviews.php"


//COLORS
#define RGB(r, g, b) [UIColor colorWithRed:(float)r / 255.0 green:(float)g / 255.0 blue:(float)b / 255.0 alpha:1.0]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(float)r / 255.0 green:(float)g / 255.0 blue:(float)b / 255.0 alpha:a]
#define GREEN_COLOR RGB(111, 182, 42)
#define ORANGE_COLOR RGB(247, 143, 30)
#define GREEN_COLOR_MENU RGB(130, 159, 92)
#define GRAY_COLOR RGB(157, 157, 157)
#define GRAY_LIGHT_COLOR RGB(218, 218, 218)
#define STAR RGB(215, 215, 215)
#define TITLE_COLOR RGB(132, 132, 132)
#define LABEL_COLOR RGB(156, 156, 156)

//FONT AND SIZE TEXT
#define FONT_NORMAL_TEXT [UIFont fontWithName:@"Century Gothic" size:16]

#define LENGTH_ZIPCODE 5
#define LENGTH_PHONE 14

//TIMER
#define T_CART_EXPIRE 86400
#define T_SHARE_MENU  5400
#define T_ICON_ORDER 1500
#define T_REFRESH_LOCATION 1500

@interface Macros : NSObject

@end
