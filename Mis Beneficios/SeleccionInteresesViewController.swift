//
//  SeleccionInteresesViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 05-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SeleccionInteresesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

     @IBOutlet weak internal var btMasTarde: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var belleza = ["fondo_belleza", "bt_belleza", "bt_belleza_selected", "false", "3"]
    var deporte = ["fondo_deporte", "bt_deporte", "bt_deportes_selected", "false", "13"]
    var hogar = ["fondo_hogar", "bt_hogar", "bt_hogar_selected", "false", "5"]
    var ropa = ["fondo_ropa", "bt_ropa", "bt_ropa_selected", "false", "4"]
    var entretencion = ["fondo_entretencion", "bt_entretencion", "bt_entretencion_selected", "false", "2"]
    var pasatiempos = ["fondo_pasatiempo", "bt_pasatiempos", "bt_pasatiempos_selected", "false", "14"]
    var gastronomia = ["fondo_gastronomia", "bt_gastronomia", "bt_gastronomia_selected", "false", "1"]
    var viajes = ["fondo_viajes", "bt_viajes", "bt_viajes_selected", "false", "6"]
    var ninos = ["fondo_ninos", "bt_ninos", "bt_ninos_selected", "false", "7"]
    var tecnologia = ["fondo_tecnologia", "bt_tecnologia", "bt_tecnologia_selected", "false", "8"]
    var varios = ["fondo_varios", "bt_varios", "bt_varios_selected", "false", "9"]
    var array = Array<Array<String>>()
    var preferencias: [Int] = []
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var convenios: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        array = [belleza, deporte,hogar, gastronomia, ropa, pasatiempos, entretencion, tecnologia, ninos, viajes, varios]
        
        if (VariablesGlobales.perfil.gustos != "")
        {
            var myStringArr = VariablesGlobales.perfil.gustos.componentsSeparatedByString(",")
            for index in 0...myStringArr.count-1 {
                preferencias.append(Int(myStringArr[index])!)
            }
        }
        if(userDefaults.boolForKey("secondTime"))
        {
            btMasTarde.hidden = true
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func showActivityIndicatory(uiView: UIView) {
        let container: UIView = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("celdaIntereses", forIndexPath: indexPath) as! CustomCellIntereses
        let id = array[indexPath.row][4]
        if(preferencias.count > 0){
            for index in 0...preferencias.count-1 {
                if Int(id) == preferencias[index] {
                    array[indexPath.row][3] = "true"
                    break
                }
            }
        }
        
        var interes = array[indexPath.row]

        if(interes[3] == "false")
        {
            cell.button.setImage(UIImage.init(named: interes[1]), forState: UIControlState.Normal)
        }
        else{
            cell.button.setImage(UIImage.init(named: interes[2]), forState: UIControlState.Normal)
        }

        cell.iwFondo.image = UIImage.init(named: interes[0])
        cell.button.tag = indexPath.row
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 82
    }
    
    
    @IBAction func checkInteres(sender: UIButton) {
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! CustomCellIntereses;
        var interes = array[indexPath.row]
        let id = interes[4]
        if(interes[3] == "false")
        {
            cell.button.setImage(UIImage.init(named: interes[2]), forState: UIControlState.Normal)
            array[indexPath.row][3] = "true"
            preferencias.append(Int(id)!)
            print(preferencias)
            
        }
        else
        {
            cell.button.setImage(UIImage.init(named: interes[1]), forState: UIControlState.Normal)
            array[indexPath.row][3] = "false"
            if(preferencias.count > 0){
                for index in 0...preferencias.count-1 {
                    if Int(id) == preferencias[index] {
                        preferencias.removeAtIndex(index)
                        print(preferencias)
                        return
                    }
                }
            }

        }
        
    }

    @IBAction func continuar(sender: AnyObject) {
        performSelectorInBackground(#selector(SeleccionInteresesViewController.showActivityIndicatory(_:)), withObject: self.view)
        userDefaults.setObject(preferencias, forKey: "prefersIntereses")
                self.userDefaults.setBool(true, forKey: "secondTime")
                self.userDefaults.synchronize()
        
        
        var gustos = ""
        
        if(preferencias.count != 0)
        {
            for index in 0...preferencias.count-1 {
                gustos = gustos + String(preferencias[index]) + ","
            }
        }
        
        if gustos != "" {
            gustos = gustos.substringToIndex(gustos.endIndex.predecessor())
        }
        
        if(self.convenios != nil)
        {
            Request().guardarGustosConvenios(VariablesGlobales.perfil.email, gustos:gustos , convenios: self.convenios!)
            VariablesGlobales.perfil.convenios = self.convenios!
        }
        else
        {
            Request().guardarGustosConvenios(VariablesGlobales.perfil.email, gustos:gustos , convenios: "")
            VariablesGlobales.perfil.gustos = gustos
        }
    
        
            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //It is instance of  `NewViewController` from storyboard
            let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
            VariablesGlobales.arrayOfertas.removeAll()
            
            vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
            self.presentViewController(vc, animated: true, completion: nil)
    }

}

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

