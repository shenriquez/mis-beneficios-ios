//
//  SeleccionLectoresViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 03-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class SeleccionLectoresViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    
     @IBOutlet weak internal var btMasTarde: UIButton!
    @IBOutlet var tableView: UITableView!
    var prefers:[Int] = []
    var mercurio = ["El Mercurio", "14"]
    var laTercera = ["La Tercera",  "15"]
     var copecLanpass = ["Copec Lanpass",  "6"]
    var array = Array<Array<String>>()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        if VariablesGlobales.convenios.count != 0 {
            
            for index in 0...VariablesGlobales.convenios.count-1 {
                if((VariablesGlobales.convenios[index].dictionaryValue["tipo"]!.string) == "4"){
                    
                    let tarjeta = [VariablesGlobales.convenios[index].dictionaryValue["nombre_convenio"]!.string!, VariablesGlobales.convenios[index].dictionaryValue["id_convenio"]!.string!]
                    
                    array.append(tarjeta)
                }
            }
        }
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersLectores") {
            if arraySave.count != 0 {
                prefers = arraySave as! [Int]
            }
        }
              
        if(userDefaults.boolForKey("secondTime"))
        {
            btMasTarde.hidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("celdaBancos", forIndexPath: indexPath) as! CustomCell
        var banco = array[indexPath.row]
        cell.txtContenido.text = banco[0]
        cell.uiSwitch.tag = indexPath.row
        if(prefers.count > 0){
              let id = banco[1]
            for index in 0...prefers.count-1 {
                if Int(id) == prefers[index] {
                    cell.uiSwitch.on = true
                    cell.txtContenido.textColor = COLOR_BLUE
                    cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
                    break
                }
            }
        }
        
        return cell
        
    }
    
    @IBAction func chanceValue(sender: UISwitch) {
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! CustomCell;
        if sender.on {
            cell.txtContenido.textColor = COLOR_BLUE
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
            var banco = array[sender.tag]
            let id = banco[1]
            prefers.append(Int(id)!)
            print(prefers)
        }
        else{
            cell.txtContenido.textColor = UIColor.init(red:CGFloat(70/255.0), green: CGFloat(70/255.0), blue: CGFloat(70/255.0), alpha: 1.0)
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightLight)
            var banco = array[sender.tag]
            let id = banco[1]
            if(prefers.count > 0){
                for index in 0...prefers.count-1 {
                    if Int(id) == prefers[index] {
                        prefers.removeAtIndex(index)
                        print(prefers)
                        return
                    }
                }
            }
            
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    
    func showActivityIndicatory(uiView: UIView) {
        let container: UIView = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "irPerfil"
        {
            VariablesGlobales.arrayOfertas.removeAll()
        }
        else if segue.identifier == "continuarIntereses"
        {
            let viewController:SeleccionInteresesViewController = segue.destinationViewController as! SeleccionInteresesViewController
            viewController.convenios = sender as? String
        }
 
    }
    
    @IBAction func savePrefers(sender: AnyObject) {
        userDefaults.setObject(prefers, forKey: "prefersLectores")
        userDefaults.synchronize()
        var cuentas = ""
        var cuentasGuardadas:[Int] = []
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersBanco") {
            if arraySave.count != 0 {
                cuentasGuardadas = arraySave as! [Int]
                for index in 0...cuentasGuardadas.count-1 {
                    cuentas = cuentas +  String(cuentasGuardadas[index]) + ","
                }
                
            }
        }
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersTarjetas") {
            if arraySave.count != 0 {
                cuentasGuardadas = arraySave as! [Int]
                for index in 0...cuentasGuardadas.count-1 {
                    cuentas = cuentas + String(cuentasGuardadas[index]) + ","
                }
                
            }
        }
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersTelefonia") {
            if arraySave.count != 0 {
                cuentasGuardadas = arraySave as! [Int]
                for index in 0...cuentasGuardadas.count-1 {
                    cuentas = cuentas + String(cuentasGuardadas[index]) + ","
                }
                
            }
        }
        
        if(prefers.count != 0)
        {
            for index in 0...prefers.count-1 {
                cuentas = cuentas + String(prefers[index]) + ","
            }
            
        }
        
        
        if cuentas != "" {
            cuentas = cuentas.substringToIndex(cuentas.endIndex.predecessor())
        }
        
        if(userDefaults.boolForKey("secondTime"))
        {
            performSelectorInBackground(#selector(SeleccionLectoresViewController.showActivityIndicatory(_:)), withObject: self.view)
            
            
            Request().guardarGustosConvenios(VariablesGlobales.perfil.email, gustos:"" , convenios: cuentas)
            VariablesGlobales.perfil.convenios = cuentas
            performSegueWithIdentifier("irPerfil", sender: nil)
        }
        else
        {
           
            performSegueWithIdentifier("continuarIntereses", sender: cuentas)
            
        }
    }
}
