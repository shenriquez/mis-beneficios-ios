//
//  SeleccionTelefoniaViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 03-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class SeleccionTelefoniaViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
     @IBOutlet weak internal var btMasTarde: UIButton!
    @IBOutlet var tableView: UITableView!
    var prefers:[Int] = []
    var entel = ["Entel", "11"]
    var movistar = ["Movistar",  "13"]
    var claro = ["Claro",  "12"]
    var array = Array<Array<String>>()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        if VariablesGlobales.convenios.count != 0 {
            
            for index in 0...VariablesGlobales.convenios.count-1 {
                if((VariablesGlobales.convenios[index].dictionaryValue["tipo"]!.string) == "3"){
                    
                    let tarjeta = [VariablesGlobales.convenios[index].dictionaryValue["nombre_convenio"]!.string!, VariablesGlobales.convenios[index].dictionaryValue["id_convenio"]!.string!]
                    
                    array.append(tarjeta)
                }
            }
        }

        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersTelefonia") {
            if arraySave.count != 0 {
                prefers = arraySave as! [Int]
            }
        }
       
        if(userDefaults.boolForKey("secondTime"))
        {
            btMasTarde.hidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("celdaBancos", forIndexPath: indexPath) as! CustomCell
        var banco = array[indexPath.row]
        cell.txtContenido.text = banco[0]
        cell.uiSwitch.tag = indexPath.row
        let id = banco[1]
        if(prefers.count > 0){
            for index in 0...prefers.count-1 {
                if Int(id) == prefers[index] {
                    cell.uiSwitch.on = true
                    cell.txtContenido.textColor = COLOR_BLUE
                    cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
                    break
                }
            }
        }
        
        return cell
        
    }
    
    @IBAction func chanceValue(sender: UISwitch) {
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! CustomCell;
        if sender.on {
            cell.txtContenido.textColor = COLOR_BLUE
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
            var banco = array[sender.tag]
            let id = banco[1]
            prefers.append(Int(id)!)
            print(prefers)
        }
        else{
            cell.txtContenido.textColor = UIColor.init(red:CGFloat(70/255.0), green: CGFloat(70/255.0), blue: CGFloat(70/255.0), alpha: 1.0)
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightLight)
            var banco = array[sender.tag]
            let id = banco[1]
            if(prefers.count > 0){
                for index in 0...prefers.count-1 {
                    if Int(id) == prefers[index] {
                        prefers.removeAtIndex(index)
                        print(prefers)
                        return
                    }
                }
            }
            
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "continuarSubscripciones"
        {
            if segue.destinationViewController is SeleccionLectoresViewController {
            }
        }
    }
    
    @IBAction func savePrefers(sender: AnyObject) {
        
        userDefaults.setObject(prefers, forKey: "prefersTelefonia")
        userDefaults.synchronize()
        performSegueWithIdentifier("continuarSubscripciones", sender: nil)
    }
}

