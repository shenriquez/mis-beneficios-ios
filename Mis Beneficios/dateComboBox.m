//
//  dateComboBox.m
//  BancaMobilIphone
//
//  Created by Medianet on 19/11/14.
//  Copyright (c) 2014 com.provincial.bancamovil2. All rights reserved.
//


#import "dateComboBox.h"

@implementation dateComboBox





- (id)init
{
    self = [super init];
    if (self) {

        NSLocale * aca = [[NSLocale alloc]initWithLocaleIdentifier:@"es_ES"];
        [self setLocale:aca];
        [self setDate:[NSDate date]];
        self.datePickerMode = UIDatePickerModeDate;

        
        NSDate *currentDate = [NSDate date];

    
        [self setMinimumDate:currentDate];
        
        UITapGestureRecognizer * gsto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDateTapGestureRecognized:)];
        gsto.delegate = self;
        [self addGestureRecognizer:gsto];
        [self addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];

        
        
    }
    return self;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return NO;
}


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    NSString * clase = NSStringFromClass([otherGestureRecognizer.view class]);
    
    
    
    if ([clase isEqualToString:@"UIPickerTableViewWrapperCell"])
    {
        NSLog(@"taped yea");
        taped = YES;
        return YES;
    }
    else
    {
        return NO;
    }
}
-(void)pickerViewDateTapGestureRecognized:(UITapGestureRecognizer *)sender
{
    CGPoint toque = [sender locationInView:self];
    
    CGFloat topearriba = 124.000000;
    CGFloat topeabajo = 92.000000;
    
    
    if (toque.y < topearriba && toque.y > topeabajo)
    {
        NSLog(@"bien, cerrando");
        [[_textField superview]  endEditing:YES];
        [self updateTextField:nil];
        [self dateComboBoxCerradoConFecha:_fechaEnvio];
    }
    
    
}
- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"dd'/'MM'/'yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
    
}

-(void)serasVistaDeEntradaParaUITextField:(UITextField*)t
{
    _textField = t;
    [_textField setInputView:self];
    [_textField setDelegate:self];
}

-(void)updateTextField:(id)sender
{
        _fechaMuestra = [self formatDate:self.date];
        _textField.text = _fechaMuestra;
        _fechaEnvio = [_fechaMuestra stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        

    if (taped)
    {
        [[_textField superview]  endEditing:YES];
        [self dateComboBoxCerradoConFecha:_fechaEnvio];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    taped = NO;
}
-(void)dateComboBoxCerradoConFecha:(NSString*)f
{
    taped = NO;
    if (self.delegado && [self.delegado respondsToSelector:@selector(dateComboBoxCerradoConFecha:)])
    {
        [self.delegado dateComboBoxCerradoConFecha:f];
    }

}
@end
