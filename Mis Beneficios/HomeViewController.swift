//
//  HomeViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 06-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HomeViewController: UITableViewController{
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var cuentas = ""
    var gustosGuardados:[Int] = []
    var cuentasGuardadas:[Int] = []
   
    override func viewDidLoad() {
              
        super.viewDidLoad()
        
      self.tableView.registerNib(UINib(nibName: "CustomCellDescuento", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellDescuentos")

        tableView.estimatedRowHeight = 60.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = true
        tableView.separatorStyle = .None
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(self.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        self.refreshOfertas()

    }
    
    func refreshOfertas(){
 
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersBanco") {
            if arraySave.count != 0 {
                cuentasGuardadas = arraySave as! [Int]
                for index in 0...cuentasGuardadas.count-1 {
                    cuentas = String(cuentasGuardadas[index]) + ","
                }
                
            }
        }
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersTarjetas") {
            if arraySave.count != 0 {
                cuentasGuardadas = arraySave as! [Int]
                for index in 0...cuentasGuardadas.count-1 {
                    cuentas = cuentas + String(cuentasGuardadas[index]) + ","
                }
                
            }
        }
        
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersLectores") {
            if arraySave.count != 0 {
                cuentasGuardadas = arraySave as! [Int]
                for index in 0...cuentasGuardadas.count-1 {
                    cuentas = cuentas + String(cuentasGuardadas[index]) + ","
                }
                
            }
        }
        
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersTelefonia") {
            if arraySave.count != 0 {
                cuentasGuardadas = arraySave as! [Int]
                for index in 0...cuentasGuardadas.count-1 {
                    cuentas = cuentas + String(cuentasGuardadas[index]) + ","
                }
                
            }
        }
        
        var gustos = ""
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersIntereses") {
            if arraySave.count != 0 {
                gustosGuardados = arraySave as! [Int]
                for index in 0...gustosGuardados.count-1 {
                    gustos = gustos + String(gustosGuardados[index]) + ","
                }
                
            }
        }
        
        if cuentas != "" {
            cuentas = cuentas.substringToIndex(cuentas.endIndex.predecessor())
        }
        
        if gustos != "" {
            gustos = gustos.substringToIndex(gustos.endIndex.predecessor())
        }
        
        print(gustos, cuentas)
        
        
        Alamofire.request(.POST, WS_OFERTAS, parameters: ["gustos": gustos, "cuentas": cuentas])
            .validate()
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result {
                case .Success:
                    print("Validation Successful")
                case .Failure(let error):
                    print(error)
                }
                
                let json = JSON(data: response.data!)
                
                let ofertas = json[1].array
                if ofertas?.count != 0 {
                   VariablesGlobales.arrayOfertas.removeAll()
                    for index in 0...ofertas!.count-1 {
                        let oferta = ofertas![index]["data"].dictionaryValue
                        let objOferta = Oferta()
                        objOferta.idOferta = oferta["id_descuento"]!.intValue
                        objOferta.titulo = oferta["descuento"]!.string
                        objOferta.descripcion = oferta["descripcion"]!.string
                        objOferta.urlImagen = oferta["imagen"]!.string
                        objOferta.categoria = oferta["nombre_cat"]!.string
                        objOferta.nombreConvenio = oferta["nombre_cuenta"]!.string
                        objOferta.subCategoria = oferta["nombre_subcat"]!.string
                        
                        switch objOferta.categoria {
                        case "Gastronomia":
                            objOferta.fotoCategoria = "ic_list_gastronomia"
                        case "Entretencion":
                            objOferta.fotoCategoria = "ic_list_entretencion"
                        case "Salud y belleza":
                            objOferta.fotoCategoria = "ic_list_salud"
                        case "Ropa y calzado":
                            objOferta.fotoCategoria = "ic_list_ropa"
                        case "Decoración y hogar":
                            objOferta.fotoCategoria = "ic_list_hogar"
                        case "Viajes y turismo":
                            objOferta.fotoCategoria = "ic_list_viaje"
                        case "Niños":
                            objOferta.fotoCategoria = "ic_list_ninos"
                        case "Tecnologia":
                            objOferta.fotoCategoria = "ic_list_tecnologia"
                        case "Varios":
                            objOferta.fotoCategoria = "ic_list_varios"
                        case "Deportes":
                            objOferta.fotoCategoria = "ic_list_deporte"
                        case "Pasatiempos":
                            objOferta.fotoCategoria = "ic_list_pasatiempo"
                        default:
                            objOferta.fotoCategoria = ""
                            break
                        }

                        VariablesGlobales.arrayOfertas.append(objOferta)
                        self.tableView.reloadData()
                        self.refreshControl!.endRefreshing()
                    }
                }
                

                
        }
        

    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if(VariablesGlobales.arrayOfertas.count == 0)
      {
        return 1
        }
        return VariablesGlobales.arrayOfertas.count;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         let cell = self.tableView.dequeueReusableCellWithIdentifier("cellDescuentos", forIndexPath: indexPath) as! CustomCellHomeTableViewCell
        
        if(VariablesGlobales.arrayOfertas.count == 0)
        {
            cell.lbTitulo.text = "No se han encontraron beneficios asociados a tus gustos"
            cell.lbCuenta.text = ""
            cell.lbCategoria.text = ""
            cell.imgCategoria.image = UIImage.init(named: "alerta")
            cell.userInteractionEnabled = false
        
        }
        else
        {
            cell.lbTitulo.text = VariablesGlobales.arrayOfertas[indexPath.row].titulo
            cell.lbCuenta.text = VariablesGlobales.arrayOfertas[indexPath.row].nombreConvenio + " / "
            cell.lbCategoria.text = VariablesGlobales.arrayOfertas[indexPath.row].categoria + " - " + VariablesGlobales.arrayOfertas[indexPath.row].subCategoria
            cell.imgCategoria.image = UIImage.init(named: VariablesGlobales.arrayOfertas[indexPath.row].fotoCategoria)
        
        }
        
         return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleOfertaViewController = storyboard.instantiateViewControllerWithIdentifier("detalleOferta") as! DetalleOfertaViewController
        let oferta = VariablesGlobales.arrayOfertas[indexPath.row]
        vc.oferta = oferta
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
