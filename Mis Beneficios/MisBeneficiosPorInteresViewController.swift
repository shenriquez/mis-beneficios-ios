//
//  MisBeneficiosPorInteresViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 13-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class MisBeneficiosPorInteresViewController: UITableViewController {
    
    var arrayCategorias = Array<Categoria>()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var gustosGuardados:[Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.tableView.registerNib(UINib(nibName: "CustomCellMisBeneficios", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellCategoria")
        
        tableView.allowsSelection = true
        tableView.separatorStyle = .None
        
        if (VariablesGlobales.perfil.gustos != "")
        {
            var myStringArr = VariablesGlobales.perfil.gustos.componentsSeparatedByString(",")
            for index in 0...myStringArr.count-1 {
                gustosGuardados.append(Int(myStringArr[index])!)
            }
        }
        
        if(gustosGuardados.count != 0)
        {
            for index in 0...gustosGuardados.count-1 {
                switch gustosGuardados[index] {
                case 1:
                    let categoria = Categoria()
                    categoria.idCategoria = 1
                    categoria.nombreCategoria = "Gastronomia"
                    categoria.imgCategoria = UIImage.init(named: "ic_gastronomia")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 2:
                    let categoria = Categoria()  
                    categoria.idCategoria = 2
                    categoria.nombreCategoria = "Entretencion"
                    categoria.imgCategoria = UIImage.init(named: "ic_entretencion")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 3:
                    let categoria = Categoria()
                    categoria.idCategoria = 3
                    categoria.nombreCategoria = "Salud y belleza"
                    categoria.imgCategoria = UIImage.init(named: "ic_belleza")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 4:
                    let categoria = Categoria()
                    categoria.idCategoria = 4
                    categoria.nombreCategoria = "Ropa y calzado"
                    categoria.imgCategoria = UIImage.init(named: "ic_ropa")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 5:
                    let categoria = Categoria()
                    categoria.idCategoria = 5
                    categoria.nombreCategoria = "Decoración y hogar"
                    categoria.imgCategoria = UIImage.init(named: "ic_hogar")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 6:
                    let categoria = Categoria()
                    categoria.idCategoria = 6
                    categoria.nombreCategoria = "Viajes y turismo"
                    categoria.imgCategoria = UIImage.init(named: "ic_viajes")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 7:
                    let categoria = Categoria()
                    categoria.idCategoria = 7
                    categoria.nombreCategoria = "Niños"
                    categoria.imgCategoria = UIImage.init(named: "ic_ninos")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 8:
                    let categoria = Categoria()
                    categoria.idCategoria = 8
                    categoria.nombreCategoria = "Tecnología"
                    categoria.imgCategoria = UIImage.init(named: "ic_tecnologia")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 9:
                    let categoria = Categoria()
                    categoria.idCategoria = 9
                    categoria.nombreCategoria = "Varios"
                    categoria.imgCategoria = UIImage.init(named: "ic_varios")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 13:
                    let categoria = Categoria()
                    categoria.idCategoria = 13
                    categoria.nombreCategoria = "Deportes"
                    categoria.imgCategoria = UIImage.init(named: "ic_deporte")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                case 14:
                    let categoria = Categoria()
                    categoria.idCategoria = 14
                    categoria.nombreCategoria = "Pasatiempos"
                    categoria.imgCategoria = UIImage.init(named: "ic_pasatiempo")
                    categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.categoria.containsString(categoria.nombreCategoria)})
                    categoria.cantidadOfertas = categoria.ofertas.count
                    arrayCategorias.append(categoria)
                default:
                    break
                }
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String(gustosGuardados.count)
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel.init(frame: CGRectMake(16, 10, self.tableView.frame.size.width, 20))
        myLabel.font = UIFont(name: "Signika-Light", size: 15.0)!
        myLabel.text = "Tienes      intereses de 11"
        
        let myLabelnum = UILabel.init(frame: CGRectMake(60, 10, self.tableView.frame.size.width, 20))
        myLabelnum.font = UIFont(name: "Signika", size: 15.0)!
        myLabelnum.textColor = COLOR_RED
        myLabelnum.text = self.tableView(tableView, titleForHeaderInSection: section)

        
        let headerView = UIView.init();
        headerView.backgroundColor = UIColor.whiteColor()
        headerView.addSubview(myLabel)
        headerView.addSubview(myLabelnum)
        return headerView;
    }


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(arrayCategorias.count == 0)
        {
            return 1
        }
        
        return arrayCategorias.count;
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 85
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellCategoria", forIndexPath: indexPath) as! CustomCellMisBeneficios
        
        if(arrayCategorias.count == 0)
        {
            cell.lbCategoria.text = "No haz seleccionado ningún interés"
            cell.lbNumBeneficios.text = ""
            cell.userInteractionEnabled = false
            cell.accessoryType = .None
            cell.imgCategoria.image = UIImage.init(named: "alerta")
            cell.txtBeneficios.hidden = true

        }
        else
        {
            cell.lbCategoria.text = arrayCategorias[indexPath.row].nombreCategoria
            cell.lbNumBeneficios.text = String(arrayCategorias[indexPath.row].cantidadOfertas)
            cell.imgCategoria.image = arrayCategorias[indexPath.row].imgCategoria
            cell.txtBeneficios.hidden = false
            
            
            if(arrayCategorias[indexPath.row].cantidadOfertas == 0)
            {
                cell.userInteractionEnabled = false
                cell.accessoryType = .None
            }
            else
            {
                cell.userInteractionEnabled = true
                cell.accessoryType = .DisclosureIndicator
            }
        }
        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleMisBeneficiosViewController = storyboard.instantiateViewControllerWithIdentifier("detallePorInteres") as! DetalleMisBeneficiosViewController
        let categoria = arrayCategorias[indexPath.row]
        vc.categoria = categoria
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
    
    }
    
    
}
