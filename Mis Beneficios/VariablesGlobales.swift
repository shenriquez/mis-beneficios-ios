//
//  VariablesGlobales.swift
//  Mis Beneficios
//
//  Created by Sarai on 21-06-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import SwiftyJSON

class VariablesGlobales: NSObject {
    
    internal static var convenios = Array<JSON>()
    internal static var refresPerfil = true
    internal static var perfil = Perfil()
    internal static var arrayOfertas = Array<Oferta>()
    internal static var ofertaMapa: Oferta?

}
