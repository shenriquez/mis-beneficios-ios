//
//  Categoria.swift
//  Mis Beneficios
//
//  Created by Sarai on 13-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class Categoria: NSObject {
    
    var idCategoria: Int!
    var nombreCategoria: String!
    var imgCategoria: UIImage?
    var cantidadOfertas: Int!
    var ofertas: Array<Oferta> = Array<Oferta>()

}
