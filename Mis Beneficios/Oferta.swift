//
//  Ofertas.swift
//  Mis Beneficios
//
//  Created by Sarai on 09-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import MapKit

class Oferta {
    
    var idOferta: Int!
    var titulo: String?
    var descuento: String?
    var descripcion: String?
    var urlImagen: String!
    var link: String?
    var nombreEmpresa: String!
    var nombreConvenio: String!
    var categoria: String!
    var subCategoria: String!
    var fotoCategoria: String!
    var vigenciaInicio: String!
    var vigenciaFin: String!
    var ubicaciones: Array<CLLocationCoordinate2D> = []
    var rating = 0
    var favorito: Int?
    

}
