//
//  Save.swift
//  Mis Beneficios
//
//  Created by Sarai on 12-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class Save: NSObject {
    
    var ofertas: Array<Oferta>!
    
    public func setOfertas(arrayOfertas: Array<Oferta>){
        ofertas = arrayOfertas
    }
    
    public func getOfertas() -> Array<Oferta>{
        return ofertas
    }
}
