//
//  IniciarSesionViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 17-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Google
import FBSDKLoginKit

class IniciarSesionViewController: UIViewController, GIDSignInUIDelegate, FBSDKLoginButtonDelegate, GIDSignInDelegate {

    @IBOutlet  var signInFb: FBSDKLoginButton!
    @IBOutlet weak var signinGoole: GIDSignInButton!
    @IBOutlet weak var txtContrasena: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!

    var oneTime = false
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var container: UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signInFb.readPermissions =  ["public_profile", "email", "user_birthday"];
        signInFb.delegate = self
        signInFb.setTitle("Facebook", forState: .Normal)
        signinGoole.style = .Wide
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self

        
        if (userDefaults.stringForKey("fb") == "n") {
            performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            let (response, message) = Request().iniciarSesion(userDefaults.stringForKey("email")!, password: userDefaults.stringForKey("password")!, nombre: "", apellido: "", sexo: "", nacimiento: "", fb: "n", token: "")
            
            if(response == true)
            {
                let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //It is instance of  `NewViewController` from storyboard
                let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
                
                vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                self.presentViewController(vc, animated: true, completion: nil)
            }
            else
            {
                container.removeFromSuperview()
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                presentViewController(alertController, animated: true, completion: nil)
            }
            
        }
        else if (userDefaults.stringForKey("fb") == "g") {
            performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            let (_, token) = Request().iniciarSesion(userDefaults.stringForKey("email")!, password: "", nombre: "", apellido: "", sexo: "", nacimiento: "", fb: "g", token: "")
            if(token != "")
            {
                let response = Request().signInGoogle(token)
                
                if(response == true)
                {
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    //It is instance of  `NewViewController` from storyboard
                    let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
                    
                    vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                    self.presentViewController(vc, animated: true, completion: nil)
                }
                else
                {
                    container.removeFromSuperview()
                    let alertController = UIAlertController(title: "", message: "Ocurrio un error, por favor intente de nuevo", preferredStyle: .Alert)
                    
                    let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    presentViewController(alertController, animated: true, completion: nil)
                }
            }
        }
        else if(FBSDKAccessToken.currentAccessToken() != nil && userDefaults.stringForKey("fb") == "f")
        {
            performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            returnUserData()
        }
        
        VariablesGlobales.convenios = Request().getConvenios()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func iniciaSesion(sender: AnyObject) {
        
        if(txtCorreo.text != "" && !checkEmail(txtCorreo.text!)){
            
            let alertController = UIAlertController(title: "", message: "Por favor verifica tu correo electrónico", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
        else if(txtCorreo.text == ""){
            
            let alertController = UIAlertController(title: "", message: "Por favor ingresa tu correo electrónico", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
            
        }
        else if(txtContrasena.text == "")
        {
            let alertController = UIAlertController(title: "", message: "Por favor ingresa tu contraseña", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
            
        }
        else
        {
            performSelectorInBackground(#selector(IniciarSesionViewController.showActivityIndicatory(_:)), withObject: self.view)
            
            let (response, message) =  Request().iniciarSesion(txtCorreo.text!, password: txtContrasena.text!, nombre: "", apellido: "", sexo: "", nacimiento: "", fb: "n", token: "")
            
            if(response){
                
                if(self.userDefaults.boolForKey("secondTime"))
                {
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    //It is instance of  `NewViewController` from storyboard
                    let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
                    
                    vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                    self.presentViewController(vc, animated: true, completion: nil)
                }
                else
                {
                    let storyboard : UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
                    //It is instance of  `NewViewController` from storyboard
                    let vc : SeleccionBancosViewController = storyboard.instantiateViewControllerWithIdentifier("seleccion_bancos") as! SeleccionBancosViewController
                    
                    vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                    self.presentViewController(vc, animated: true, completion: nil)
                
                }
                
                self.userDefaults.setObject(self.txtCorreo.text!, forKey: "email")
                self.userDefaults.setObject(self.txtContrasena.text!, forKey: "password")
                self.userDefaults.setObject("n", forKey: "fb")
                self.userDefaults.synchronize()

            }
            else{
                
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
            }
            
             self.container.removeFromSuperview()

        }
        
    }
        
    func showActivityIndicatory(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }

    func textFieldShouldReturn(userText: UITextField) -> Bool {
        userText.resignFirstResponder()
        self.view.endEditing(true)
        return true;
    }
 
    func checkEmail(email: String) -> Bool {
        var result = false
        var atSeparetedStrings = email.componentsSeparatedByString("@")
        let atSeparetedStringsCount = atSeparetedStrings.count
        
        if atSeparetedStringsCount == 2 {
            let firstString = atSeparetedStrings[0]
            
            if(firstString.characters.count > 0 )
            {
                let otherString = atSeparetedStrings[1]
                let dotRange = otherString.rangeOfString(".")
                if(dotRange != nil)
                {
                    let dotLocation = otherString.startIndex.distanceTo(dotRange!.startIndex)
                    
                    if(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < (otherString.characters.count - 1)))
                    {
                        result = true
                    }
                }
                
            }
        }
        
        return result
    }

    
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
        
            performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            // Perform any operations on signed in user here.

            let userId = user.authentication.accessToken                 // For client-side use only!
            _ = user.authentication.idToken // Safe to send to the server
            
            let response = Request().signInGoogle(userId)
            
            if(response)
            {
                let (response, message) = Request().iniciarSesion(VariablesGlobales.perfil.email, password: "", nombre: VariablesGlobales.perfil.nombre, apellido: VariablesGlobales.perfil.apellido, sexo: VariablesGlobales.perfil.genero, nacimiento: "0000-00-00", fb: "g", token: userId)
                
                if(response){
                    
                        if(self.userDefaults.boolForKey("secondTime") )
                        {
                            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            //It is instance of  `NewViewController` from storyboard
                            let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
                            
                            vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                            self.presentViewController(vc, animated: true, completion: nil)
                        }
                        else
                        {
                            let storyboard : UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
                            //It is instance of  `NewViewController` from storyboard
                            let vc : SeleccionBancosViewController = storyboard.instantiateViewControllerWithIdentifier("seleccion") as! SeleccionBancosViewController
                            
                            vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                            self.presentViewController(vc, animated: true, completion: nil)
                            
                        }
                        
                        self.userDefaults.setObject(VariablesGlobales.perfil.email, forKey: "email")
                        self.userDefaults.setObject("g", forKey: "fb")
                        self.userDefaults.synchronize()
                    
                }
                else{
                    
                    VariablesGlobales.perfil.nombre = ""
                    VariablesGlobales.perfil.apellido = ""
                    VariablesGlobales.perfil.email = ""
                    VariablesGlobales.perfil.genero = ""
                    
                    
                    let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
                    
                    let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                }
                
                self.container.removeFromSuperview()
            }
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        print("User Logged In")
        self.returnUserData()
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
           
            if result.grantedPermissions.contains("email")
            {
                // Do work
                if(oneTime == false)
                {
                  //  oneTime = true
                    self.returnUserData()                    
                }
               
                
            }
        }
    }
    
       func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
       return true
    }
    
    func returnUserData()
    {
        performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
        oneTime = true
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,name,email, gender,birthday,first_name,last_name,picture.type(large)"])
        
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
                self.container.removeFromSuperview()
            }
            else
            {
                print("fetched user: \(result)")
                let nombre = result.valueForKey("first_name") as! String
                let apellido =  result.valueForKey("last_name") as! String
                let email = result.valueForKey("email") as! String
                let genero = result.valueForKey("gender") as! String
                let picture = result.valueForKey("picture") as! NSDictionary
                let data: NSDictionary = picture["data"] as! NSDictionary
                
             /*   let nacimiento = result.valueForKey("birthday") as! String
                
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy"
                
                let date = dateFormatter.dateFromString(nacimiento)
                
                let dateFormatter2 = NSDateFormatter()
                dateFormatter2.dateFormat = "yyyy-MM-dd"
                let dateString = dateFormatter2.stringFromDate(date!)*/
                
                var sexo:String
                if(genero == "female"){
                    sexo = "f"
                }
                else
                {
                    sexo = "m"
                }
                
                VariablesGlobales.perfil.urlFoto = data["url"] as! String
                VariablesGlobales.perfil.nombre = nombre
                VariablesGlobales.perfil.apellido = apellido
                VariablesGlobales.perfil.email = email
                VariablesGlobales.perfil.genero = genero
                
                let (response,message) = Request().iniciarSesion(email, password: "", nombre: nombre, apellido: apellido, sexo: sexo, nacimiento: "0000-00-00", fb: "f", token: FBSDKAccessToken.currentAccessToken().tokenString)
                
                if(response){
                    
                    if(self.userDefaults.boolForKey("secondTime"))
                    {
                        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        //It is instance of  `NewViewController` from storyboard
                        let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
                        
                        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                        self.presentViewController(vc, animated: true, completion: nil)
                    }
                    else
                    {
                        let storyboard : UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
                        //It is instance of  `NewViewController` from storyboard
                        let vc : SeleccionBancosViewController = storyboard.instantiateViewControllerWithIdentifier("seleccion") as! SeleccionBancosViewController
                        
                        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                        self.presentViewController(vc, animated: true, completion: nil)
                        
                    }
                    
                    self.userDefaults.setObject(email, forKey: "email")
                    self.userDefaults.setObject("f", forKey: "fb")
                    self.userDefaults.synchronize()

                }
                else{
                    
                    VariablesGlobales.perfil.nombre = ""
                    VariablesGlobales.perfil.apellido = ""
                    VariablesGlobales.perfil.email = ""
                    VariablesGlobales.perfil.genero = ""
                    
                    
                    let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
                    
                    let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                }
                
                self.container.removeFromSuperview()

            }
            
        })
    }
    

        


  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
