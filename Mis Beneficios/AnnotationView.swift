//
//  AnnotationView.swift
//  Mis Beneficios
//
//  Created by Sarai on 08-06-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import MapKit

class AnnotationView: MKAnnotationView {
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, withEvent: event)
        if(hitView != nil){
            self.superview!.bringSubviewToFront(self)
        }
        return hitView
    }
    
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        let rect = self.bounds
        let isInside = CGRectContainsRect(rect, point)
    }
    
    
    
    
   
}
