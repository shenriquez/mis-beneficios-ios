//
//  Request.swift
//  Mis Beneficios
//
//  Created by Sarai on 09-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FBSDKLoginKit
import MapKit


class Request {
    
    func obtenerOfertas(gustos: String, convenios: String, email: String)  -> Array<Oferta> {
        
        var arrayOfertas = Array<Oferta>()
        
        if Reachability.isConnectedToNetwork() == true {
            
            
            let urlString = "gustos=" + gustos + "&convenios=" + convenios + "&email=" + email
            
            print("url" + urlString)
            
            let url: NSURL = NSURL(string: WS_OFERTAS)!
            let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
            
            request.HTTPMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
            
            let session = NSURLSession.sharedSession()
            let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)
            
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                
                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                    print("Dim background error")
                    
                    return
                }
                
                if(data != nil)
                {
                    let json = JSON(data: data!)
                    print(json)
                    
                    if(json != nil)
                    {
                        let ofertas = json["data"].array
                        if(ofertas != nil)
                        {
                            if ofertas?.count != 0 {
                                arrayOfertas.removeAll()
                                for index in 0...ofertas!.count-1 {
                                    let oferta = ofertas![index].dictionaryValue
                                    let objOferta = Oferta()
                                    objOferta.idOferta = oferta["id_descuento"]!.intValue
                                    objOferta.titulo = oferta["nombre"]!.string
                                    objOferta.descuento = oferta["descuento"]!.string
                                    objOferta.descripcion = oferta["descripcion"]!.string
                                    objOferta.urlImagen = oferta["imagen"]!.string
                                    objOferta.link = oferta["link"]!.string
                                    objOferta.nombreEmpresa = oferta["nombre_empresa"]!.string
                                    objOferta.nombreConvenio = oferta["nombre_convenio"]!.string
                                    objOferta.categoria = oferta["nombre_cat"]!.string
                                    objOferta.subCategoria = oferta["nombre_subcat"]!.string
                                    objOferta.vigenciaInicio = oferta["vigencia_inicio"]!.string
                                    objOferta.vigenciaFin = oferta["vigencia_fin"]!.string
                                    
                                    if(( oferta["rating"]!.int) != nil)
                                    {
                                        objOferta.rating = oferta["rating"]!.int!
                                    }
                                    
                                    objOferta.favorito = oferta["favorito"]!.int
                                    
                                    
                                    var ubicaciones = oferta["ubicaciones"]?.array
                                    
                                    for index in 0...ubicaciones!.count-1 {
                                        objOferta.ubicaciones.append(CLLocationCoordinate2D(latitude: ubicaciones![index]["lat"].doubleValue, longitude: ubicaciones![index]["lon"].doubleValue))
                                    }
                                    
                                    
                                    switch objOferta.categoria {
                                    case "Gastronomia":
                                        objOferta.fotoCategoria = "ic_list_gastronomia"
                                    case "Entretencion":
                                        objOferta.fotoCategoria = "ic_list_entretencion"
                                    case "Salud y belleza":
                                        objOferta.fotoCategoria = "ic_list_salud"
                                    case "Ropa y calzado":
                                        objOferta.fotoCategoria = "ic_list_ropa"
                                    case "Decoración y hogar":
                                        objOferta.fotoCategoria = "ic_list_hogar"
                                    case "Viajes y turismo":
                                        objOferta.fotoCategoria = "ic_list_viaje"
                                    case "Niños":
                                        objOferta.fotoCategoria = "ic_list_ninos"
                                    case "Tecnologia":
                                        objOferta.fotoCategoria = "ic_list_tecnologia"
                                    case "Varios":
                                        objOferta.fotoCategoria = "ic_list_varios"
                                    case "Deportes":
                                        objOferta.fotoCategoria = "ic_list_deporte"
                                    case "Pasatiempos":
                                        objOferta.fotoCategoria = "ic_list_pasatiempo"
                                    default:
                                        objOferta.fotoCategoria = ""
                                        break
                                    }
                                    
                                    arrayOfertas.append(objOferta)
                                }
                            }
                        }
                        
                    }
                }
                dispatch_semaphore_signal(semaphore);
            }
            
            task.resume()
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
            
            return arrayOfertas
        } else {
            print("Internet connection FAILED")
            return arrayOfertas
        }
    }
    
    
    func obtenerDetellaOferta(email: String, id_descuento: Int)  -> Oferta {
        
        let objOferta = Oferta()
        
        let urlString = "email=" + email + "&id_descuento=" + String(id_descuento)
        
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_DETALLE_OFERTA)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                print(json)
                
                if(json != nil)
                {
                    let oferta = json["data"].dictionary
                    if(oferta != nil)
                    {
                        
                        objOferta.idOferta = oferta!["id_descuento"]!.intValue
                        objOferta.titulo = oferta!["nombre"]!.string
                        objOferta.descuento = oferta!["descuento"]!.string
                        objOferta.descripcion = oferta!["descripcion"]!.string
                        objOferta.urlImagen = oferta!["imagen"]!.string
                        objOferta.link = oferta!["link"]!.string
                        objOferta.nombreEmpresa = oferta!["nombre_empresa"]!.string
                        objOferta.nombreConvenio = oferta!["nombre_convenio"]!.string
                        objOferta.categoria = oferta!["nombre_cat"]!.string
                        objOferta.subCategoria = oferta!["nombre_subcat"]!.string
                        objOferta.vigenciaInicio = oferta!["vigencia_inicio"]!.string
                        objOferta.vigenciaFin = oferta!["vigencia_fin"]!.string
                        if(( oferta!["rating"]!.int) != nil)
                        {
                            objOferta.rating = oferta!["rating"]!.int!
                        }
                        
                        
                        var ubicaciones = oferta!["ubicaciones"]?.array
                        
                        for index in 0...ubicaciones!.count-1 {
                            objOferta.ubicaciones.append(CLLocationCoordinate2D(latitude: ubicaciones![index]["lat"].doubleValue, longitude: ubicaciones![index]["lon"].doubleValue))
                        }
                        
                        
                        switch objOferta.categoria {
                        case "Gastronomia":
                            objOferta.fotoCategoria = "ic_list_gastronomia"
                        case "Entretencion":
                            objOferta.fotoCategoria = "ic_list_entretencion"
                        case "Salud y belleza":
                            objOferta.fotoCategoria = "ic_list_salud"
                        case "Ropa y calzado":
                            objOferta.fotoCategoria = "ic_list_ropa"
                        case "Decoración y hogar":
                            objOferta.fotoCategoria = "ic_list_hogar"
                        case "Viajes y turismo":
                            objOferta.fotoCategoria = "ic_list_viaje"
                        case "Niños":
                            objOferta.fotoCategoria = "ic_list_ninos"
                        case "Tecnologia":
                            objOferta.fotoCategoria = "ic_list_tecnologia"
                        case "Varios":
                            objOferta.fotoCategoria = "ic_list_varios"
                        case "Deportes":
                            objOferta.fotoCategoria = "ic_list_deporte"
                        case "Pasatiempos":
                            objOferta.fotoCategoria = "ic_list_pasatiempo"
                        default:
                            objOferta.fotoCategoria = ""
                            break
                        }
                        
                        
                    }
                    
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        return objOferta
        
    }
    
    func agregarFavorito(email: String, id_descuento: Int) -> Bool  {
        var boolResponse = false
        let urlString = "email=" + email + "&id_descuento=" + String(id_descuento)
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_ADD_FAVORITO)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {
                        boolResponse = true
                    }
                    else
                    {
                        boolResponse = false
                    }
                }
                else{
                    
                    boolResponse = false
                    
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        return boolResponse
    }
    
    func eliminarFavorito(email: String, id_descuento: Int) -> Bool  {
        var boolResponse = false
        let urlString = "email=" + email + "&id_descuento=" + String(id_descuento)
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_DELETE_FAVORITO)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {
                        boolResponse = true
                    }
                    else
                    {
                        boolResponse = false
                    }
               }
                else{
                    
                    boolResponse = false
                    
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        return boolResponse
    }
    
    func compartir(email: String, id_descuento: Int) -> Bool  {
        var boolResponse = false
        let urlString = "email=" + email + "&id_descuento=" + String(id_descuento)
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_COMPARTIR)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {}
                }
                else{
                    
                    boolResponse = false
                    
                }
            }
        }
        
        task.resume()

        return boolResponse
    }

    
    
    func recordar(email: String, id_descuento: Int)  {
        let urlString = "email=" + email + "&id_descuento=" + String(id_descuento)
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_RECORDAR)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {
                    }

                }
                else{

                    
                }
            }
        }
        
        task.resume()

    }
    
    func agregarRating(email: String, id_descuento: Int, rating: Float) -> Bool  {
        var boolResponse = false
        let urlString = "email=" + email + "&id_descuento=" + String(id_descuento) + "&rating=" + String(rating)

        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_ADD_RATING)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)
        
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {
                        boolResponse = true
                    }
                    else
                    {
                        boolResponse = false
                    }
                    
                }
                else{
                    
                    boolResponse = false
                    
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        return boolResponse
    }

    
    func registrar(email: String, password: String, nombre: String, apellido: String, sexo: String, nacimiento: String, fb: String) -> (Bool, String)  {
        
        var boolResponse = false
        var stringResponse = ""
        
        let urlString = "email=" + email + "&password=" + password + "&nombre=" + nombre + "&apellido_p=" + apellido + "&sexo=" + sexo + "&nacimiento=" + nacimiento + "&fb=" + fb
        
        
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_REGISTRO)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {
                        boolResponse = true
                    }
                    else
                    {
                        boolResponse = false
                        stringResponse = json["message"].string!

                    }
                    
                }
                else{
                    
                    boolResponse = false
                    stringResponse = "Ocurrio un error, por favor intente de nuevo"
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        return (boolResponse, stringResponse)
    }
    
    
    
    func iniciarSesion(email: String, password: String, nombre: String, apellido: String, sexo: String, nacimiento: String, fb: String, token: String)  -> (Bool,String) {
        
        var boolResponse = false
        var stringResponse = ""
        var urlString: String
        
        if Reachability.isConnectedToNetwork() {
            if(fb != "n")
            {
                urlString = "email=" + email + "&nombre=" + nombre + "&apellido_p=" + apellido + "&sexo=" + sexo + "&nacimiento=" + nacimiento + "&fb=" + fb + "&token=" + token
                
            }
            else{
                urlString = "email=" + email + "&password=" + password + "&fb=" + fb
            }
            
            
            print("url" + urlString)
            
            let url: NSURL = NSURL(string: WS_LOGIN)!
            let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
            
            request.HTTPMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
            
            let session = NSURLSession.sharedSession()
            let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)
            
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                
                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                    print("Dim background error")
                    return
                }
                
                if(data != nil)
                {
                    let json = JSON(data: data!)
                    
                    print(json)
                    
                    if(json != nil)
                    {
                        let status = json["status"].string
                        
                        if(status == "true"){
                            
                            VariablesGlobales.perfil.nombre = json["data"].dictionary!["nombre"]!.string!
                            VariablesGlobales.perfil.apellido = json["data"].dictionary!["apellido_p"]!.string!
                            VariablesGlobales.perfil.nacimiento = json["data"].dictionary!["nacimiento"]!.string!
                            VariablesGlobales.perfil.email = email
                            
                            if(json["data"].dictionaryValue["gustos"]?.string != nil){
                                VariablesGlobales.perfil.gustos = json["data"].dictionaryValue["gustos"]!.string!
                            }
                            
                            if(json["data"].dictionaryValue["convenios"]?.string != nil){
                                VariablesGlobales.perfil.convenios = json["data"].dictionaryValue["convenios"]!.string!
                            }
                            
                            if(fb != "n"){
                                boolResponse = true
                                stringResponse = json["data"].dictionary!["token"]!.string!
                            }
                            else{
                                boolResponse = true
                            }
                            
                        }
                        else{
                            boolResponse = false
                            stringResponse = json["message"].stringValue
                        }
                    }
                    else{
                        
                        boolResponse = false
                        stringResponse = "Ocurrio un error, por favor intente de nuevo"
                    }
                }
                dispatch_semaphore_signal(semaphore);
            }
            
            task.resume()
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
            return (boolResponse, stringResponse)
        }else{
            return (boolResponse,stringResponse )
        }
        
        
    }
    
    func signInGoogle(token: String) -> Bool {
        
        var boolResponse = false
        
        let url = NSURL(string:  "https://www.googleapis.com/oauth2/v3/userinfo?access_token=\(token)")
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        
        request.HTTPMethod = "GET"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let error = json["error"].string
                    if(error != "" && error != nil)
                    {
                        boolResponse = false
                    }
                    
                    let nombre = json["given_name"].string
                    let apellido = json["family_name"].string
                    let email = json["email"].string
                    let genero = json["gender"].string
                    var sexo:String
                    
                    
                    if(genero == "female")
                    {
                        sexo = "f"
                    }
                    else
                    {
                        sexo = "m"
                    }
                    
                    VariablesGlobales.perfil.nombre = nombre!
                    VariablesGlobales.perfil.apellido = apellido!
                    VariablesGlobales.perfil.email = email!
                    VariablesGlobales.perfil.genero = sexo
                    
                    boolResponse = true
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        return boolResponse
    }
    
    
    func guardarGustosConvenios(email: String, gustos: String, convenios:String ) -> Bool  {
        
        var boolResponse = false
        
        var urlString: String
        
        if(gustos != "" && convenios != "")
        {
            urlString = "email=" + email + "&gustos=" + gustos + "&convenios=" + convenios
        }
        else if (gustos == ""){
            urlString = "email=" + email + "&convenios=" + convenios
        }
        else
        {
            urlString = "email=" + email + "&gustos=" + gustos
        }
        
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_SAVE_PREFERENCIAS)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {
                        boolResponse = true
                    }
                    else
                    {
                        boolResponse = false
                        
                    }
                }
                else{
                    
                    boolResponse = false
                    
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        return boolResponse
    }
    
    func getConvenios() -> Array<JSON> {
        
        let url: NSURL = NSURL(string: WS_GET_CONVENIOS)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "GET"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        var convenios = Array<JSON>()
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                dispatch_semaphore_signal(semaphore);
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                
                let json = JSON(data: data!)
                convenios = json["data"].array!
                
                print(json)
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

        return convenios
        
    }
   

    func getPerfil(email: String)  {
        var arrayFavoritos = Array<Oferta>()
        var arrayRecordados = Array<Oferta>()
        var arrayCompartidos = Array<Oferta>()
        let urlString = "email=" + email
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_PERFIL)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)

        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                print(json)
                let jsonData = json["data"].dictionary
                if(jsonData != nil)
                {
                    /*  perfil.nombre = json["perfil"].dictionary!["nombre"]!.string!
                     perfil.apellido = json["perfil"].dictionary!["apellido_p"]!.string!
                     perfil.gustos = json["perfil"].dictionary!["gustos"]!.string!
                     perfil.convenios = json["perfil"].dictionary!["cuentas"]!.string!
                     perfil.email = json["perfil"].dictionary!["email"]!.string!
                     */
                    
                    let favoritos = jsonData!["favoritos"]!.array
                    if(favoritos != nil)
                    {
                        if favoritos?.count != 0 {
                            for index in 0...favoritos!.count-1 {
                                let oferta = favoritos![index].dictionaryValue
                                let objOferta = Oferta()
                                objOferta.idOferta = oferta["id_descuento"]!.intValue
                                objOferta.titulo = oferta["nombre"]!.string
                                objOferta.descuento = oferta["descuento"]!.string
                                objOferta.nombreConvenio = oferta["nombre_convenio"]!.string
                                objOferta.categoria = oferta["nombre_cat"]!.string
                                objOferta.subCategoria = oferta["nombre_subcat"]!.string
                                
                                
                                switch objOferta.categoria {
                                case "Gastronomia":
                                    objOferta.fotoCategoria = "ic_list_gastronomia"
                                case "Entretencion":
                                    objOferta.fotoCategoria = "ic_list_entretencion"
                                case "Salud y belleza":
                                    objOferta.fotoCategoria = "ic_list_salud"
                                case "Ropa y calzado":
                                    objOferta.fotoCategoria = "ic_list_ropa"
                                case "Decoración y hogar":
                                    objOferta.fotoCategoria = "ic_list_hogar"
                                case "Viajes y turismo":
                                    objOferta.fotoCategoria = "ic_list_viaje"
                                case "Niños":
                                    objOferta.fotoCategoria = "ic_list_ninos"
                                case "Tecnologia":
                                    objOferta.fotoCategoria = "ic_list_tecnologia"
                                case "Varios":
                                    objOferta.fotoCategoria = "ic_list_varios"
                                case "Deportes":
                                    objOferta.fotoCategoria = "ic_list_deporte"
                                case "Pasatiempos":
                                    objOferta.fotoCategoria = "ic_list_pasatiempo"
                                default:
                                    objOferta.fotoCategoria = ""
                                    break
                                }
                                
                                arrayFavoritos.append(objOferta)
                            }
                            
                            VariablesGlobales.perfil.favoritos = arrayFavoritos
                            VariablesGlobales.perfil.numFavoritos = arrayFavoritos.count
                        }
                    }
                    
                    
                    let compartidos = jsonData!["compartidos"]!.array
                    if(compartidos != nil)
                    {
                        if compartidos?.count != 0 {
                            for index in 0...compartidos!.count-1 {
                                let oferta = compartidos![index].dictionaryValue
                                let objOferta = Oferta()
                                objOferta.idOferta = oferta["id_descuento"]!.intValue
                                objOferta.titulo = oferta["nombre"]!.string
                                objOferta.descuento = oferta["descuento"]!.string
                                objOferta.nombreConvenio = oferta["nombre_convenio"]!.string
                                objOferta.categoria = oferta["nombre_cat"]!.string
                                objOferta.subCategoria = oferta["nombre_subcat"]!.string
                                
                                
                                switch objOferta.categoria {
                                case "Gastronomia":
                                    objOferta.fotoCategoria = "ic_list_gastronomia"
                                case "Entretencion":
                                    objOferta.fotoCategoria = "ic_list_entretencion"
                                case "Salud y belleza":
                                    objOferta.fotoCategoria = "ic_list_salud"
                                case "Ropa y calzado":
                                    objOferta.fotoCategoria = "ic_list_ropa"
                                case "Decoración y hogar":
                                    objOferta.fotoCategoria = "ic_list_hogar"
                                case "Viajes y turismo":
                                    objOferta.fotoCategoria = "ic_list_viaje"
                                case "Niños":
                                    objOferta.fotoCategoria = "ic_list_ninos"
                                case "Tecnologia":
                                    objOferta.fotoCategoria = "ic_list_tecnologia"
                                case "Varios":
                                    objOferta.fotoCategoria = "ic_list_varios"
                                case "Deportes":
                                    objOferta.fotoCategoria = "ic_list_deporte"
                                case "Pasatiempos":
                                    objOferta.fotoCategoria = "ic_list_pasatiempo"
                                default:
                                    objOferta.fotoCategoria = ""
                                    break
                                }
                                
                                
                                arrayCompartidos.append(objOferta)
                            }
                            
                            VariablesGlobales.perfil.compartidos = arrayCompartidos
                            VariablesGlobales.perfil.numCompartidos = arrayCompartidos.count
                        }
                    }
                    
                    let recordados = jsonData!["recordados"]!.array
                    
                    if(recordados != nil)
                    {
                        if recordados?.count != 0 {
                            for index in 0...recordados!.count-1 {
                                let oferta = recordados![index].dictionaryValue
                                let objOferta = Oferta()
                                objOferta.idOferta = oferta["id_descuento"]!.intValue
                                objOferta.titulo = oferta["nombre"]!.string
                                objOferta.descuento = oferta["descuento"]!.string
                                objOferta.nombreConvenio = oferta["nombre_convenio"]!.string
                                objOferta.categoria = oferta["nombre_cat"]!.string
                                objOferta.subCategoria = oferta["nombre_subcat"]!.string
                                
                                
                                switch objOferta.categoria {
                                case "Gastronomia":
                                    objOferta.fotoCategoria = "ic_list_gastronomia"
                                case "Entretencion":
                                    objOferta.fotoCategoria = "ic_list_entretencion"
                                case "Salud y belleza":
                                    objOferta.fotoCategoria = "ic_list_salud"
                                case "Ropa y calzado":
                                    objOferta.fotoCategoria = "ic_list_ropa"
                                case "Decoración y hogar":
                                    objOferta.fotoCategoria = "ic_list_hogar"
                                case "Viajes y turismo":
                                    objOferta.fotoCategoria = "ic_list_viaje"
                                case "Niños":
                                    objOferta.fotoCategoria = "ic_list_ninos"
                                case "Tecnologia":
                                    objOferta.fotoCategoria = "ic_list_tecnologia"
                                case "Varios":
                                    objOferta.fotoCategoria = "ic_list_varios"
                                case "Deportes":
                                    objOferta.fotoCategoria = "ic_list_deporte"
                                case "Pasatiempos":
                                    objOferta.fotoCategoria = "ic_list_pasatiempo"
                                default:
                                    objOferta.fotoCategoria = ""
                                    break
                                }
                                
                                
                                
                                arrayRecordados.append(objOferta)
                            }
                            
                            VariablesGlobales.perfil.recordados = arrayRecordados
                            VariablesGlobales.perfil.numRecordados = arrayRecordados.count
                        }
                    }
                    
                    
                    
                }
            
            }
            dispatch_semaphore_signal(semaphore);

        }
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)

    }
    
    func actualizarPerfil(email: String, password: String, nombre: String, apellido: String, sexo: String, nacimiento: String, fb: String) -> (Bool, String)  {
        
        var boolResponse = false
        var stringResponse = ""
        var urlString = ""
        
        if(password != "")
        {
            urlString = "email=" + email + "&password=" + password + "&nombre=" + nombre + "&apellido_p=" + apellido + "&sexo=" + sexo + "&nacimiento=" + nacimiento + "&fb=" + fb
        }
        else
        {
            urlString = "email=" + email + "&nombre=" + nombre + "&apellido_p=" + apellido + "&sexo=" + sexo + "&nacimiento=" + nacimiento + "&fb=" + fb
        }
        
        print("url" + urlString)
        
        let url: NSURL = NSURL(string: WS_UPDATE_PERFIL)!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = urlString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let session = NSURLSession.sharedSession()
        let semaphore: dispatch_semaphore_t = dispatch_semaphore_create(0)
        
        let task = session.dataTaskWithRequest(request) {
            (let data, let response, let error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                print("Dim background error")
                return
            }
            
            if(data != nil)
            {
                let json = JSON(data: data!)
                
                print(json)
                
                if(json != nil)
                {
                    let status = json["status"].string
                    if(status == "true")
                    {
                        boolResponse = true
                    }
                    else
                    {
                        boolResponse = false
                        stringResponse = json["message"].string!
                        
                    }
                    
                }
                else{
                    
                    boolResponse = false
                    stringResponse = "Ocurrio un error, por favor intente de nuevo"
                }
            }
            dispatch_semaphore_signal(semaphore);
        }
        
        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        return (boolResponse, stringResponse)
    }

    
}
        

