//
//  Perfil.swift
//  Mis Beneficios
//
//  Created by Sarai on 5/26/16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class Perfil {

    var nombre = ""
    var apellido = ""
    var email = ""
    var nacimiento = ""
    var gustos = ""
    var convenios = ""
    var genero = ""
    var urlFoto = ""
    var favoritos = Array<Oferta>()
    var numFavoritos: Int = 0
    var compartidos = Array<Oferta>()
    var numCompartidos: Int = 0
    var recordados = Array<Oferta>()
    var numRecordados: Int = 0
    var numConvenios: Int = 0
    var numIntereses: Int = 0
            
}

