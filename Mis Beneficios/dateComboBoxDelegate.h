//
//  dateComboBoxDelegate.h
//  BancaMobilIphone
//
//  Created by Medianet on 19/11/14.
//  Copyright (c) 2014 com.provincial.bancamovil2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constantes_Macros.h"

@protocol dateComboBoxDelegate <NSObject>

-(void)dateComboBoxCerradoConFecha:(NSString*)f;
@end
