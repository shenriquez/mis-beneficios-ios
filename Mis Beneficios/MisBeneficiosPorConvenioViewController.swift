//
//  MisBeneficiosPorConvenioViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 06-06-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class MisBeneficiosPorConvenioViewController: UITableViewController {

    var arrayCategorias = Array<Categoria>()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var conveniosGuardados:[Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().barTintColor = UIColor.redColor()
        
        
        self.tableView.registerNib(UINib(nibName: "CustomCellMisBeneficios", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellCategoria")
        
        tableView.allowsSelection = true
        tableView.separatorStyle = .None
        
        if (VariablesGlobales.perfil.convenios != "")
        {
            var myStringArr = VariablesGlobales.perfil.convenios.componentsSeparatedByString(",")
            for index in 0...myStringArr.count-1 {
                conveniosGuardados.append(Int(myStringArr[index])!)
            }
        }
        
        if VariablesGlobales.convenios.count != 0 {
            
            if(conveniosGuardados.count != 0)
            {
                for index in 0...conveniosGuardados.count-1 {
                    for index1 in 0...VariablesGlobales.convenios.count-1 {
                        if((VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue) == conveniosGuardados[index]){
                            
                            let categoria = Categoria()
                            categoria.idCategoria = VariablesGlobales.convenios[index1].dictionaryValue["id_convenio"]!.intValue
                            categoria.nombreCategoria = VariablesGlobales.convenios[index1].dictionaryValue["nombre_convenio"]!.stringValue
                            
                            switch VariablesGlobales.convenios[index1].dictionaryValue["tipo"]!.intValue {
                            case 1:
                                categoria.imgCategoria = UIImage.init(named: "ic_list_convenio_banco")
                            case 2:
                                categoria.imgCategoria = UIImage.init(named: "ic_list_convenio_tarjeta")
                            case 3:
                                categoria.imgCategoria = UIImage.init(named: "ic_list_convenio_telefono")
                            case 4:
                                categoria.imgCategoria = UIImage.init(named: "ic_list_convenio_varios")
                            default:
                                categoria.imgCategoria = UIImage.init(named: "ic_list_convenio_varios")
                            }
                            
                            categoria.ofertas = VariablesGlobales.arrayOfertas.filter({$0.nombreConvenio.containsString(categoria.nombreCategoria)})
                            categoria.cantidadOfertas = categoria.ofertas.count
                            arrayCategorias.append(categoria)
                            break
                        }
                    }
                    
                }
            }
        }        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String(conveniosGuardados.count)
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel.init(frame: CGRectMake(16, 10, self.tableView.frame.size.width, 20))
        myLabel.font = UIFont(name: "Signika-Light", size: 15.0)!
        myLabel.text = "Tienes      convenios de 23"
        
        let myLabelnum = UILabel.init(frame: CGRectMake(59, 10, self.tableView.frame.size.width, 20))
        myLabelnum.font = UIFont(name: "Signika", size: 15.0)!
        myLabelnum.textColor = COLOR_RED
        myLabelnum.text = self.tableView(tableView, titleForHeaderInSection: section)
        
        
        let headerView = UIView.init();
        headerView.backgroundColor = UIColor.whiteColor()
        headerView.addSubview(myLabel)
        headerView.addSubview(myLabelnum)
        return headerView;
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(arrayCategorias.count == 0)
        {
            return 1
        }
        
        return arrayCategorias.count;
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 85
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellCategoria", forIndexPath: indexPath) as! CustomCellMisBeneficios
        
        if(arrayCategorias.count == 0)
        {
            cell.lbCategoria.text = "No haz seleccionado ningún club de beneficio"
            cell.lbNumBeneficios.text = ""
            cell.userInteractionEnabled = false
            cell.accessoryType = .None
            cell.imgCategoria.image = UIImage.init(named: "alerta")
            cell.txtBeneficios.hidden = true
            
        }
        else
        {
            cell.lbCategoria.text = arrayCategorias[indexPath.row].nombreCategoria
            cell.lbNumBeneficios.text = String(arrayCategorias[indexPath.row].cantidadOfertas)
            cell.imgCategoria.image = arrayCategorias[indexPath.row].imgCategoria
            cell.txtBeneficios.hidden = false
            
            if(arrayCategorias[indexPath.row].cantidadOfertas == 0)
            {
                cell.userInteractionEnabled = false
                cell.accessoryType = .None
            }
            else
            {
                cell.userInteractionEnabled = true
                cell.accessoryType = .DisclosureIndicator
            }
            
        }
        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleMisBeneficiosViewController = storyboard.instantiateViewControllerWithIdentifier("detallePorInteres") as! DetalleMisBeneficiosViewController
        let categoria = arrayCategorias[indexPath.row]
        vc.categoria = categoria
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
        
        
        
    }
    
}
