//
//  PerfilViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 18-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Google
import SDWebImage
import FBSDKLoginKit

class PerfilViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var imgPerfil: UIImageView!
    @IBOutlet weak var lbInteresesSeleccionados: UILabel!
    @IBOutlet weak var lbBeneficiosCompartidos: UILabel!
    @IBOutlet weak var lbBeneficiosFavoritos: UILabel!
    @IBOutlet weak var lbConveniosSeleccionados: UILabel!
    @IBOutlet weak var lbNombre: UILabel!
    @IBOutlet weak var lbBeneficiosGuardados: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    var scrollView: UIScrollView!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var container: UIView = UIView()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgPerfil.layer.masksToBounds = true;
        imgPerfil.layer.cornerRadius = 40.0;
        imgPerfil.layer.rasterizationScale = UIScreen.mainScreen().scale
        imgPerfil.layer.shouldRasterize = true;
        imgPerfil.clipsToBounds = true;

        if(GIDSignIn.sharedInstance().currentUser != nil)
        {
            if(GIDSignIn.sharedInstance().currentUser.profile.hasImage)
            {
                let dimension: UInt = UInt.init(imgPerfil.frame.size.height * UIScreen.mainScreen().scale)
                let imgUrl = GIDSignIn.sharedInstance().currentUser.profile .imageURLWithDimension(dimension)
                imgPerfil.sd_setImageWithURL(imgUrl)
                
            }
        }
        else if (FBSDKAccessToken.currentAccessToken() != nil && VariablesGlobales.perfil.urlFoto != "" )
        {
            imgPerfil.sd_setImageWithURL(NSURL.init(string:VariablesGlobales.perfil.urlFoto), placeholderImage: UIImage.init(named: "unknown"))
        
        }
        else if(userDefaults.stringForKey("fb") == "n")
        {
            let possibleOldImagePath = userDefaults.objectForKey("imagePerfil") as! String?
            if let oldImagePath = possibleOldImagePath {
                let oldFullPath = self.documentsPathForFileName(oldImagePath)
                let oldImageData = NSData(contentsOfFile: oldFullPath)
                // here is your saved image:
                let oldImage = UIImage(data: oldImageData!)
                imgPerfil.image = oldImage
            }
            imgPerfil.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(PerfilViewController.loadImageButtonTapped)))
            imagePicker.delegate = self
        
        }
        
        scrollView = UIScrollView(frame: CGRectMake(0, 79, self.view.frame.size.width, self.view.frame.size.height))
        scrollView.backgroundColor = UIColor.whiteColor()
        scrollView.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        viewContent.frame.size.width = self.view.frame.size.width
        scrollView.addSubview(viewContent)
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 880)
        view.addSubview(scrollView)
        


        // Do any additional setup after loading the view.
    }
    

    
    
     func loadImageButtonTapped() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePicker.allowsEditing = false
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print(info[UIImagePickerControllerReferenceURL])
            self.imgPerfil.contentMode = .ScaleAspectFill
            self.imgPerfil.image = pickedImage
            
            let imageData = UIImageJPEGRepresentation(pickedImage, 1)
            let relativePath = "image_\(NSDate.timeIntervalSinceReferenceDate()).jpg"
            let path = self.documentsPathForFileName(relativePath)
            imageData!.writeToFile(path, atomically: true)
            
            userDefaults.setObject(relativePath , forKey: "imagePerfil")
            userDefaults.synchronize()
            
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func documentsPathForFileName(name: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true);
        let path = paths[0] as String;
        let fullPath = path + "/" + name
        
        return fullPath
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker = UIImagePickerController()
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    override func viewWillAppear(animated: Bool) {
         showInfoPerfil()
    }
    
    @IBAction func contactanos(sender: AnyObject) {
    }
 
    @IBAction func cerrarSesion(sender: AnyObject) {
        
        if(GIDSignIn.sharedInstance().currentUser != nil)
        {
            GIDSignIn.sharedInstance().disconnect()          
        
        }
        else if(FBSDKAccessToken.currentAccessToken() != nil){
            
            FBSDKAccessToken.setCurrentAccessToken(nil)
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }
        
        userDefaults.removeObjectForKey("email")
        userDefaults.removeObjectForKey("password")
        userDefaults.removeObjectForKey("fb")
        userDefaults.removeObjectForKey("imagePerfil")
        
        
        VariablesGlobales.perfil = Perfil()
        VariablesGlobales.arrayOfertas = Array<Oferta>()

        
        let storyboard : UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : IniciarSesionViewController = storyboard.instantiateViewControllerWithIdentifier("login") as! IniciarSesionViewController
        
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
         super.viewDidAppear(animated)
        showInfoPerfil()
    }
    
    func showInfoPerfil()  {
        
        lbNombre.text = VariablesGlobales.perfil.nombre + " " + VariablesGlobales.perfil.apellido
        lbEmail.text = VariablesGlobales.perfil.email
        
        if(VariablesGlobales.refresPerfil)
        {
            VariablesGlobales.refresPerfil = false
            Request().getPerfil(VariablesGlobales.perfil.email)
            TabBarController.container.removeFromSuperview();
        }
        
        var numConvenios = 0
        var numGustos = 0
        var gustos:[Int] = []
        var convenios:[Int] = []
        if (VariablesGlobales.perfil.gustos != "")
        {
            var myStringArr = VariablesGlobales.perfil.gustos.componentsSeparatedByString(",")
            for index in 0...myStringArr.count-1 {
                gustos.append(Int(myStringArr[index])!)
            }
            numGustos = gustos.count
        }
        if (VariablesGlobales.perfil.convenios != "")
        {
            var myStringArr = VariablesGlobales.perfil.convenios.componentsSeparatedByString(",")
            for index in 0...myStringArr.count-1 {
                convenios.append(Int(myStringArr[index])!)
            }
            
            numConvenios = convenios.count
        }

        
        lbBeneficiosFavoritos.text = String(VariablesGlobales.perfil.numFavoritos)
        lbBeneficiosGuardados.text = String(VariablesGlobales.perfil.numRecordados)
        lbBeneficiosCompartidos.text = String(VariablesGlobales.perfil.numCompartidos)
        VariablesGlobales.perfil.numConvenios = numConvenios
        VariablesGlobales.perfil.numIntereses = numGustos
        lbConveniosSeleccionados.text = String(VariablesGlobales.perfil.numConvenios)
        lbInteresesSeleccionados.text = String(VariablesGlobales.perfil.numIntereses)
        
    }
    
    @IBAction func favoritos(sender: AnyObject) {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
        vc.goTo = 1
        BaseHomeViewController.goTo = 0
        
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
        
    }

    
    @IBAction func compartidos(sender: AnyObject) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
        vc.goTo = 1
        BaseHomeViewController.goTo = 1
        
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)

    }
    @IBAction func guardados(sender: AnyObject) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
        vc.goTo = 1
        BaseHomeViewController.goTo = 2
        
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)

        
    }
    
    @IBAction func contacto(sender: AnyObject) {
        let email = "contacto@misbeneficios.cl"
        let url = NSURL(string: "mailto:\(email)")
        UIApplication.sharedApplication().openURL(url!)
    }
        
      func showActivityIndicatory(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
