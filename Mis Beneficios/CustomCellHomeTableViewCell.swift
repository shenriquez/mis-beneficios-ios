//
//  CustomCellHomeTableViewCell.swift
//  Mis Beneficios
//
//  Created by Sarai on 06-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class CustomCellHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var imgFavorito: UIImageView!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbCuenta: UILabel!
    @IBOutlet weak var lbCategoria: UILabel!
    @IBOutlet weak var imgCategoria: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
