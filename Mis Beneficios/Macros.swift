//
//  Macros.swift
//  Mis Beneficios
//
//  Created by Sarai on 09-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import Foundation
import UIKit

//WS
/*
let WS_OFERTAS = "http://dromos.cl/wsdescuentos/descuentos.php"
let WS_REGISTRO = "http://dromos.cl/wsdescuentos/registro.php"
let WS_LOGIN = "http://dromos.cl/wsdescuentos/login.php"
let WS_FAVORITOS = "http://dromos.cl/wsdescuentos/favoritos.php"
let WS_COMPARTIR = "http://dromos.cl/wsdescuentos/compartir.php"
let WS_RECORDAR = "http://dromos.cl/wsdescuentos/recordar.php"
let WS_PERFIL = "http://dromos.cl/wsdescuentos/perfil.php"
let WS_PREFERENCIAS = "http://dromos.cl/wsdescuentos/guardaGustos.php"*/

let WS_OFERTAS = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=descuentows/getall"
let WS_DETALLE_OFERTA = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=descuentows/getone"
let WS_REGISTRO = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/iniciarsesion"
let WS_LOGIN = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/iniciarsesion"
let WS_ADD_FAVORITO = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/addfavorito"
let WS_DELETE_FAVORITO = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/deletefavorito"
let WS_COMPARTIR = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/addcompartir"
let WS_RECORDAR = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/addrecordar"
let WS_PERFIL = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/getperfil"
let WS_GET_PREFERENCIAS = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/getconveniosygustos"
let WS_SAVE_PREFERENCIAS = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/saveconveniosygustos"
let WS_GET_CONVENIOS = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=descuentows/getconvenios"
let WS_ADD_RATING = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/addrating"
let WS_UPDATE_PERFIL = "http://192.168.0.184:8888/mis-beneficios-dashboard/web/index.php?r=usuariows/actualizarperfil"

let COLOR_BLUE = UIColor.init(red:CGFloat(39/255.0), green: CGFloat(172/255.0), blue: CGFloat(176/255.0), alpha: 1.0)
let COLOR_RED = UIColor.init(red:CGFloat(223/255.0), green: CGFloat(69/255.0), blue: CGFloat(51/255.0), alpha: 1.0)