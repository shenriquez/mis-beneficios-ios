//
//  CercanosMapaViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 13-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class CercanosMapaViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate  {

    @IBOutlet var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var arrayOfertasCercanas = Array<Oferta>()
    var detalleOferta: Oferta?
    let rango:CLLocationDistance = 2500
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.mapView.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
               // Do any additional setup after loading the view.
       
        arrayOfertasCercanas = VariablesGlobales.arrayOfertas
        
        
        let status = CLLocationManager.authorizationStatus()
        if(status == CLAuthorizationStatus.AuthorizedWhenInUse) {
             centerMapOnLocation(locationManager.location!)
             mapView.showsUserLocation = true
            
            addRadiusCircle(locationManager.location!)
        }
        else
        {
            self.centerMapOnLocation(CLLocation(latitude: -33.4727879, longitude: -70.6298313))
        }
        
        if(detalleOferta != nil)
        {
            for index in 0...detalleOferta!.ubicaciones.count-1 {
                let annotation = Annotation(title: detalleOferta!.titulo! + " " + detalleOferta!.descuento!, subtitle: detalleOferta!.nombreConvenio + " / " + detalleOferta!.categoria + " - " + detalleOferta!.subCategoria, categoria: detalleOferta!.categoria, coordinate: detalleOferta!.ubicaciones[index], index: detalleOferta!.idOferta!)
                self.mapView.addAnnotation(annotation)
            }
            
            self.centerMapOnLocation(CLLocation(latitude: detalleOferta!.ubicaciones[0].latitude, longitude: detalleOferta!.ubicaciones[0].longitude))
        }
        
    }
    
    func addRadiusCircle(location: CLLocation){
        let overlays = mapView.overlays
        if (overlays.count != 0) {
            mapView.removeOverlays(overlays)            
        }
        let circle = MKCircle(centerCoordinate: location.coordinate, radius: rango as CLLocationDistance)
        self.mapView.addOverlay(circle)
        
        if(arrayOfertasCercanas.count != 0)
        {
            for index in 0...arrayOfertasCercanas.count-1 {
                for index1 in 0...arrayOfertasCercanas[index].ubicaciones.count-1 {
                    let coordenada = CLLocation.init(latitude: arrayOfertasCercanas[index].ubicaciones[index1].latitude, longitude: arrayOfertasCercanas[index].ubicaciones[index1].longitude)
                    let  distance: CLLocationDistance = coordenada.distanceFromLocation(location)
                    print(Int(distance))
                    if(distance <= rango){
                            let annotation = Annotation(title: arrayOfertasCercanas[index].titulo! + " " + arrayOfertasCercanas[index].descuento!, subtitle: arrayOfertasCercanas[index].nombreConvenio + " / " + arrayOfertasCercanas[index].categoria + " - " + arrayOfertasCercanas[index].subCategoria, categoria: arrayOfertasCercanas[index].categoria, coordinate: arrayOfertasCercanas[index].ubicaciones[index1], index:  arrayOfertasCercanas[index].idOferta)
                            self.mapView.addAnnotation(annotation)
                    }
                }
            }
        }

    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = COLOR_BLUE
            circle.fillColor = COLOR_BLUE.colorWithAlphaComponent(0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return MKPolylineRenderer()
        }
    }
   
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Annotation {
            let identifier = "pin"
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
                { // 2
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                // 3
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.image = getImageCategoria(annotation.categoria)
                view.frame.size = CGSize(width: 35.0, height: 35.0)
                view.canShowCallout = true
                view.rightCalloutAccessoryView = UIButton(type: UIButtonType.InfoDark)
                view.tag = annotation.index
            }
            

            return view
        }
        return nil
    }
    
   func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : DetalleOfertaViewController = storyboard.instantiateViewControllerWithIdentifier("detalleOferta") as! DetalleOfertaViewController
    
        for index in 0...arrayOfertasCercanas.count-1 {
            if(arrayOfertasCercanas[index].idOferta == view.tag)
            {
                vc.oferta = arrayOfertasCercanas[index]
                break
            }
        }

        vc.arrayOfertas = arrayOfertasCercanas
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)
    }


    override func viewDidAppear(animated: Bool) {
        
       
    }
    
    let regionRadius: CLLocationDistance = 2000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if(status == .Denied || status == .NotDetermined)
        {
            if(detalleOferta == nil)
            {
                self.centerMapOnLocation(CLLocation(latitude: -33.4727879, longitude: -70.6298313))
            }
        }
        else if(status == .AuthorizedWhenInUse) {
            
            if(detalleOferta == nil)
            {
                centerMapOnLocation(CLLocation(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!))
            }
             mapView.showsUserLocation = true
             addRadiusCircle(locationManager.location!)
        }
    }
    
    func getImageCategoria(categoria: String) -> UIImage {
        let image1 = UIImage.init()
        switch categoria {
        case "Gastronomia":
            return UIImage.init(named: "ic_gastronomia")!
        case "Entretencion":
            return UIImage.init(named: "ic_entretencion")!
        case "Salud y belleza":
            return UIImage.init(named: "ic_belleza")!
        case "Ropa y calzado":
            return UIImage.init(named: "ic_ropa")!
        case "Decoración y hogar":
            return UIImage.init(named: "ic_hogar")!
        case "Viajes y turismo":
            return UIImage.init(named: "ic_viajes")!
        case "Niños":
            return UIImage.init(named: "ic_ninos")!
        case "Tecnología":
            return UIImage.init(named: "ic_tecnologia")!
        case "Varios":
            return UIImage.init(named: "ic_varios")!
        case "Deportes":
            return UIImage.init(named: "ic_deporte")!
        case "Pasatiempos":
           return UIImage.init(named: "ic_pasatiempo")!
        default:
            break
        }
        
        return image1
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
