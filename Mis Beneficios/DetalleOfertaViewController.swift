//
//  DetalleOfertaViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 11-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import SDWebImage
import EventKit

class DetalleOfertaViewController: UIViewController, UIScrollViewDelegate, FloatRatingViewDelegate  {


    @IBOutlet weak var btUrl: UIButton!
    @IBOutlet weak var floatRating: FloatRatingView!
    @IBOutlet weak var imgOferta: UIImageView!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbNombreCuenta: UILabel!
    @IBOutlet weak var lbCategoria: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lbDescripcion: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lbVigenciaInicio: UILabel!
    @IBOutlet weak var lbVigenciaFin: UILabel!
    var oferta: Oferta!
    var arrayOfertas = Array<Oferta>()
    var container: UIView = UIView()   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        performSelectorInBackground(#selector(self.detalleOferta), withObject: self.view)

        // Required float rating view params
        self.floatRating.emptyImage = UIImage(named: "StarEmpty")
        self.floatRating.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRating.delegate = self
        self.floatRating.contentMode = UIViewContentMode.ScaleAspectFit
        self.floatRating.maxRating = 5
        self.floatRating.minRating = 1
        self.floatRating.editable = true
        self.floatRating.halfRatings = false
        self.floatRating.floatRatings = false
       

        
        contentView.frame.size.width = self.view.frame.size.width
        scrollView.addSubview(contentView)
        
        print(contentView.frame.size.height)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {        
        contentView.frame.size.height = 228 + lbDescripcion.frame.size.height
        print(contentView.frame.size.height)
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, contentView.frame.size.height)
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
       
    }
    
    func detalleOferta()  {
        
        if(oferta.descripcion == nil)
        {
            performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            oferta =  Request().obtenerDetellaOferta(VariablesGlobales.perfil.email, id_descuento: oferta.idOferta)
        }
        
        lbTitulo.text = oferta.titulo! + " " + oferta.descuento!
        lbNombreCuenta.text = oferta.nombreConvenio + " /"
        lbCategoria.text = oferta.categoria + " - " + oferta.subCategoria!
        lbDescripcion.text = oferta.descripcion
        btUrl.setTitle(oferta.link, forState: UIControlState.Normal)
        lbVigenciaInicio.text = "Vigente desde el " + oferta.vigenciaInicio
        lbVigenciaFin.text = "Hasta el " + oferta.vigenciaFin
        
        self.floatRating.rating = Float(oferta.rating)
        if(self.floatRating.rating != 0){
            self.floatRating.editable = false
        }
        
        if(oferta.urlImagen == nil)
        {
            imgOferta.image = UIImage.init(named: "sin_imagen")
            
        }
        else
        {
            imgOferta.sd_setImageWithURL(NSURL.init(string: oferta.urlImagen), placeholderImage:UIImage.init(named: "cargando") )
        }
        
        container.removeFromSuperview()
    
    }
  
    
    @IBAction func favorito(sender: AnyObject) {
        VariablesGlobales.refresPerfil = true
        performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
        let response = Request().agregarFavorito(VariablesGlobales.perfil.email, id_descuento: oferta.idOferta)
       
                
        if(response)
        {
            VariablesGlobales.arrayOfertas = Request().obtenerOfertas(VariablesGlobales.perfil.gustos, convenios: VariablesGlobales.perfil.convenios, email: VariablesGlobales.perfil.email)
            let alertController = UIAlertController(title: "", message: "Este beneficio se ha agregado a tus favoritos exitosamente", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
            
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Este beneficio ya se encuentra agregado a tus favoritos", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)

        }
         container.removeFromSuperview()
        
    }
    
    
    @IBAction func guardar(sender: AnyObject) {
         VariablesGlobales.refresPerfil = true
        performSelectorInBackground(#selector(self.guardarRecordar), withObject: nil)
        let interval = NSDate().timeIntervalSinceReferenceDate
        let url = NSURL(string: "calshow:\(interval)")!
        UIApplication.sharedApplication().openURL(url)
     //  UIApplication.sharedApplication().openURL(NSURL(string: "calshow://")!)
    }
    
    
    @IBAction func compartir(sender: AnyObject) {
        VariablesGlobales.refresPerfil = true
        performSelectorInBackground(#selector(DetalleOfertaViewController.guardarCompatir), withObject: nil)
        let textToShare = oferta.titulo! + " " + oferta!.descuento! +  "\n" + oferta.nombreConvenio + " / " + oferta.categoria
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = sender as? UIView
        self.presentViewController(activityVC, animated: false, completion: nil)
       
    }
    
    func guardarCompatir() {
        Request().compartir(VariablesGlobales.perfil.email, id_descuento: oferta.idOferta)

    }
    
    func guardarRecordar() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat =  "yyyy-MM-dd"
        let dateInicio = dateFormatter.dateFromString(oferta!.vigenciaInicio)
        let dateFin = dateFormatter.dateFromString(oferta!.vigenciaFin)
        
        addEventToCalendar(title: oferta.titulo! + " " + oferta!.descuento!, description: oferta.descripcion!, startDate: dateInicio!, endDate: dateFin!)
    }
    
    func addEventToCalendar(title title: String, description: String?, startDate: NSDate, endDate: NSDate, completion: ((success: Bool, error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
    
        
        eventStore.requestAccessToEntityType(.Event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.saveEvent(event, span: .ThisEvent)
                     Request().recordar(VariablesGlobales.perfil.email, id_descuento: self.oferta.idOferta)
                } catch let e as NSError {
                    completion?(success: false, error: e)
                    return
                }
                completion?(success: true, error: nil)
            } else {
                completion?(success: false, error: error)
            }
            
            
        })
    }
    
    @IBAction func back(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
   
    // MARK: FloatRatingViewDelegate
    

    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {
        if(self.oferta.rating == 0){
            performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            let response = Request().agregarRating(VariablesGlobales.perfil.email, id_descuento: oferta.idOferta, rating: rating)
            print(rating)
            
            if(response)
            {
                VariablesGlobales.arrayOfertas = Request().obtenerOfertas(VariablesGlobales.perfil.gustos, convenios: VariablesGlobales.perfil.convenios, email: VariablesGlobales.perfil.email)
                let alertController = UIAlertController(title: "", message: "Gracias por calificar este beneficio", preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                presentViewController(alertController, animated: true, completion: nil)
                self.oferta.rating = Int(rating)
                self.floatRating.editable = false

            }
            else
            {
                let alertController = UIAlertController(title: "", message: "Ocurrio un error, por favor intenta de nuevo", preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                presentViewController(alertController, animated: true, completion: nil)
                
            }
            
            container.removeFromSuperview()
        }      
    }
    
    @IBAction func verMapa(sender: AnyObject) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //It is instance of  `NewViewController` from storyboard
        let vc : TabBarController = storyboard.instantiateViewControllerWithIdentifier("main") as! TabBarController
        vc.goTo = 0
        BaseMisBeneficiosViewController.goTo = 2
        VariablesGlobales.ofertaMapa = oferta        
        
        vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        self.presentViewController(vc, animated: true, completion: nil)

    }

    @IBAction func verMas(sender: AnyObject) {
        
        UIApplication.sharedApplication().openURL(NSURL(string: oferta.link!)!)
    }
    
    func showActivityIndicatory(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
