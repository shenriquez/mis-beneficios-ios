//
//  SeleccionTarjetasViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 03-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class SeleccionTarjetasViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
     @IBOutlet weak internal var btMasTarde: UIButton!
    @IBOutlet var tableView: UITableView!
    var prefers:[Int] = []
    var visa = ["Visa", "5"]
    var masterCard = ["MasterCard",  "4"]
    var ripley = ["Ripley", "8"]
    var cmr = ["CMR",  "7"]
    var cencosud = ["Cencosud", "9"]
    var presto = ["Presto", "10"]
    var array = Array<Array<String>>()
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        if VariablesGlobales.convenios.count != 0 {
            
            for index in 0...VariablesGlobales.convenios.count-1 {
                if((VariablesGlobales.convenios[index].dictionaryValue["tipo"]!.string) == "2"){
                    
                    let tarjeta = [VariablesGlobales.convenios[index].dictionaryValue["nombre_convenio"]!.string!, VariablesGlobales.convenios[index].dictionaryValue["id_convenio"]!.string!]
                    
                    array.append(tarjeta)
                }
            }
        }

        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersTarjetas") {
            if arraySave.count != 0 {
                prefers = arraySave as! [Int]
            }
        }
        
        if(userDefaults.boolForKey("secondTime"))
        {
            btMasTarde.hidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0)
        {
            return 2
        }
        else
        {
            return array.count-2
        }
        
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("celdaBancos", forIndexPath: indexPath) as! CustomCell
        var tarjeta:Array<String> = Array<String>()
        if(indexPath.section == 1){
            tarjeta = array[indexPath.row+2]
            cell.uiSwitch.tag = indexPath.row+2
        }
        else{
            tarjeta = array[indexPath.row]
            cell.uiSwitch.tag = indexPath.row
        }
        cell.txtContenido.text = tarjeta[0]
        let id = tarjeta[1]
        if(prefers.count > 0){
            for index in 0...prefers.count-1 {
                if Int(id) == prefers[index] {
                    cell.uiSwitch.on = true
                    cell.txtContenido.textColor = COLOR_BLUE
                    cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
                    break
                }else{
                    cell.uiSwitch.on = false
                    cell.txtContenido.textColor = UIColor.init(red:CGFloat(70/255.0), green: CGFloat(70/255.0), blue: CGFloat(70/255.0), alpha: 1.0)
                    cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightLight)
                }

            }
        }
        
        return cell
        
    }
    
    @IBAction func chanceValue(sender: UISwitch) {
        var indexPath: NSIndexPath
        print(sender.tag)
        if(sender.tag >= 2)
        {
            indexPath = NSIndexPath(forRow: sender.tag-2, inSection: 1)
        }
        else
        {
            indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        }

        let cell = tableView.cellForRowAtIndexPath(indexPath) as! CustomCell;
        if sender.on {
            cell.txtContenido.textColor = COLOR_BLUE
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
            var tarjeta = array[sender.tag]
            let id = tarjeta[1]
            prefers.append(Int(id)!)
            print(prefers)
        }
        else{
            cell.txtContenido.textColor = UIColor.init(red:CGFloat(70/255.0), green: CGFloat(70/255.0), blue: CGFloat(70/255.0), alpha: 1.0)
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightLight)
            var tarjeta = array[sender.tag]
            let id = tarjeta[1]
            if(prefers.count > 0){
                
                for index in 0...prefers.count-1 {
                    if Int(id) == prefers[index] {
                        prefers.removeAtIndex(index)
                        print(prefers)
                        return
                    }
                }
            }
            
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "Tarjetas bancarias"
        }
        
        return "Tarjetas de casas comerciales"
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel.init(frame: CGRectMake(20, 5, 320, 20))
        myLabel.font = UIFont(name: "Signika", size: 17.0)!
        myLabel.textColor = UIColor.whiteColor()
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        
        let headerView = UIView.init();
        headerView.backgroundColor = COLOR_BLUE
         headerView.addSubview(myLabel)
        
        return headerView;
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "continuarTelefonia"
        {
            if segue.destinationViewController is SeleccionLectoresViewController {
            }
        }
    }
    
    @IBAction func savePrefers(sender: AnyObject) {
        
        userDefaults.setObject(prefers, forKey: "prefersTarjetas")
        userDefaults.synchronize()
        performSegueWithIdentifier("continuarTelefonia", sender: nil)
    }
}