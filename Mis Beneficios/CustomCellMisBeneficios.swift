//
//  CustomCellMisBeneficios.swift
//  Mis Beneficios
//
//  Created by Sarai on 13-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class CustomCellMisBeneficios: UITableViewCell {

    @IBOutlet weak var txtBeneficios: UILabel!
    @IBOutlet weak var lbCategoria: UILabel!
    @IBOutlet weak var lbNumBeneficios: UILabel!
    @IBOutlet weak var imgCategoria: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
