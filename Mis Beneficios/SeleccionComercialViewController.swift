//
//  SeleccionComercialViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 03-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit

class SeleccionComercialViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet var tableView: UITableView!
    var prefers:[Int] = []
    var ripley = ["Ripley", "8"]
    var copecLanpass = ["Copec Lanpass",  "6"]
    var cmr = ["CMR",  "7"]
    var cencosud = ["Cencosud", "9"]
    var presto = ["Presto", "10"]
    var array = Array<Array<String>>()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.tableFooterView = UIView(frame: CGRectZero)
        array = [ripley, copecLanpass, cmr, cencosud, presto]
        
        if let arraySave : AnyObject = userDefaults.objectForKey("prefersComercial") {
            if arraySave.count != 0 {
                prefers = arraySave as! [Int]
                print(prefers)
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = self.tableView.dequeueReusableCellWithIdentifier("celdaBancos", forIndexPath: indexPath) as! CustomCell
        var tarjeta = array[indexPath.row]
        cell.txtContenido.text = tarjeta[0]
        cell.uiSwitch.tag = indexPath.row
        if(prefers.count != 0){
            for var index in 0...prefers.count-1 {
                var id = tarjeta[1]
                if Int(id) == prefers[index] {
                    cell.uiSwitch.on = true
                    cell.txtContenido.textColor = UIColor.init(red:CGFloat(39/255.0), green: CGFloat(172/255.0), blue: CGFloat(176/255.0), alpha: 1.0)
                    cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
                    break
                }else{
                    cell.uiSwitch.on = false
                    cell.txtContenido.textColor = UIColor.init(red:CGFloat(70/255.0), green: CGFloat(70/255.0), blue: CGFloat(70/255.0), alpha: 1.0)
                    cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightLight)
                }

            }
        }
        
        return cell
        
    }
    
    @IBAction func chanceValue(sender: UISwitch) {
        var indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        var cell = tableView.cellForRowAtIndexPath(indexPath) as! CustomCell;
        if sender.on {
            cell.txtContenido.textColor = UIColor.init(red:CGFloat(39/255.0), green: CGFloat(172/255.0), blue: CGFloat(176/255.0), alpha: 1.0)
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightBold)
            var tarjeta = array[sender.tag]
            var id = tarjeta[1]
            prefers.append(Int(id)!)
            print(prefers)
        }
        else{
            cell.txtContenido.textColor = UIColor.init(red:CGFloat(70/255.0), green: CGFloat(70/255.0), blue: CGFloat(70/255.0), alpha: 1.0)
            cell.txtContenido.font = UIFont.systemFontOfSize(15, weight: UIFontWeightLight)
            var tarjeta = array[sender.tag]
            var id = tarjeta[1]
            for var index in 0...prefers.count-1 {
                if Int(id) == prefers[index] {
                    prefers.removeAtIndex(index)
                    print(prefers)
                    return
                }
                
            }
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "continuarLectores"
        {
            if let destinationVC = segue.destinationViewController as? SeleccionLectoresViewController {
            }
        }
    }
    
    @IBAction func savePrefers(sender: AnyObject) {
        
        userDefaults.setObject(prefers, forKey: "prefersComercial")
        userDefaults.synchronize()
        performSegueWithIdentifier("continuarLectores", sender: nil)
    }
}
