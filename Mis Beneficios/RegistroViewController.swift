//
//  RegistroViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 16-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegistroViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellido: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtContrasena: UITextField!
    @IBOutlet weak var pickerDate: UIDatePicker!
    @IBOutlet weak var segmentedSexo: UISegmentedControl!
    var container: UIView = UIView()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerDate.maximumDate = NSDate()
        pickerDate.locale = NSLocale.init(localeIdentifier: "es_ES")
     
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registrar(sender: AnyObject) {
        
        if(txtNombre.text == "")
        {
            let alertController = UIAlertController(title: "", message: "Por favor ingresa tu nombre", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        
        }
        
        else if (txtApellido.text == "") {
            
            let alertController = UIAlertController(title: "", message: "Por favor ingresa tu apellido", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)

        }
        else if(txtCorreo.text != "" && !checkEmail(txtCorreo.text!)){
            
            let alertController = UIAlertController(title: "", message: "Por favor verifica tu correo electrónico", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
        else if(txtCorreo.text == ""){
            
            let alertController = UIAlertController(title: "", message: "Por favor ingresa tu correo electrónico", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        
        }
        else if(txtContrasena.text == "")
        {
            let alertController = UIAlertController(title: "", message: "Por favor ingresa tu contraseña", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
            
        }

        else
        {
            performSelectorInBackground(#selector(self.showActivityIndicatory(_:)), withObject: self.view)
            var sexo: String
            if(segmentedSexo.selectedSegmentIndex == 0)
            {
                sexo = "m"
            }
            else
            {
                sexo="f"
            }
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: pickerDate.date)
            
            let day = String(components.day)
            let month = String(components.month)
            let year = String(components.year)
         
            let birthday = year + "-" + month + "-" + day
            
            let (response, message) = Request().registrar(txtCorreo.text!, password: txtContrasena.text!, nombre: txtNombre.text!, apellido: txtApellido.text!, sexo: sexo, nacimiento: birthday, fb: "n")
            
            if(response){
                
                self.userDefaults.setObject(txtCorreo.text!, forKey: "email")
                self.userDefaults.setObject(txtContrasena.text!, forKey: "password")
                self.userDefaults.setObject("n", forKey: "fb")
                self.userDefaults.synchronize()
                
                VariablesGlobales.perfil.nombre =  txtNombre.text!
                VariablesGlobales.perfil.apellido = txtApellido.text!
                VariablesGlobales.perfil.email = txtCorreo.text!
                VariablesGlobales.perfil.genero = sexo
                
                let storyboard : UIStoryboard = UIStoryboard(name: "Intro", bundle: nil)
                //It is instance of  `NewViewController` from storyboard
                let vc : SeleccionBancosViewController = storyboard.instantiateViewControllerWithIdentifier("seleccion") as! SeleccionBancosViewController
                
                vc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
                self.presentViewController(vc, animated: true, completion: nil)
            }
            else{
                
                VariablesGlobales.perfil.nombre = ""
                VariablesGlobales.perfil.apellido = ""
                VariablesGlobales.perfil.email = ""
                VariablesGlobales.perfil.genero = ""
                
                
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
                
                let defaultAction = UIAlertAction(title: "Aceptar", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
            }
            
            self.container.removeFromSuperview()            
        }
    }
    
    
    func showActivityIndicatory(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.9)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }

    
    func textFieldShouldReturn(userText: UITextField) -> Bool {
        userText.resignFirstResponder()
         self.view.endEditing(true)
        return true;
    }
    
    func checkEmail(email: String) -> Bool {
        var result = false
        var atSeparetedStrings = email.componentsSeparatedByString("@")
        let atSeparetedStringsCount = atSeparetedStrings.count
        
        if atSeparetedStringsCount == 2 {
            let firstString = atSeparetedStrings[0]
            
            if(firstString.characters.count > 0 )
            {
                let otherString = atSeparetedStrings[1]
                let dotRange = otherString.rangeOfString(".")
                if(dotRange != nil)
                {
                    let dotLocation = otherString.startIndex.distanceTo(dotRange!.startIndex)
                    
                    if(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < (otherString.characters.count - 1)))
                    {
                        result = true
                    }
                }
                
            }
        }
        
        return result
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
