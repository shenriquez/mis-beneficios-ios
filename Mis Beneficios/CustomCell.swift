//
//  CustomCell.swift
//  Mis Beneficios
//
//  Created by Ramon on 02-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit



class CustomCell: UITableViewCell {

    @IBOutlet var txtContenido: UILabel!
    @IBOutlet weak var uiSwitch: UISwitch!
    @IBOutlet var estado: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        uiSwitch.onTintColor = UIColor.init(red:CGFloat(39/255.0), green: CGFloat(172/255.0), blue: CGFloat(176/255.0), alpha: 1.0)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }
}
