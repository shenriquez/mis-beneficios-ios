//
//  Annotation.swift
//  Mis Beneficios
//
//  Created by Sarai on 13-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject, MKAnnotation {
    
    let title: String!
    let subtitle: String?
    let categoria: String
    let coordinate: CLLocationCoordinate2D
    let index: Int
    
    init(title: String, subtitle: String, categoria: String, coordinate: CLLocationCoordinate2D, index: Int) {
        self.title = title
        self.subtitle = subtitle
        self.categoria = categoria
        self.coordinate = coordinate
        self.index = index
        
        super.init()
    }
   
    

}