//
//  BaseMisBeneficiosViewController.swift
//  Mis Beneficios
//
//  Created by Sarai on 13-05-16.
//  Copyright © 2016 Ramon. All rights reserved.
//

import UIKit
import PageMenu

class BaseMisBeneficiosViewController: UIViewController, CAPSPageMenuDelegate {

    
    @IBOutlet weak var btEditar: UIButton!
    @IBOutlet weak var viewPageMenu: UIView!
    var pageMenu : CAPSPageMenu?
    var actualPage: Int = 0
    internal static var goTo = 0
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let controller1 : MisBeneficiosPorInteresViewController = MisBeneficiosPorInteresViewController()
        controller1.title = "por interés"
        controllerArray.append(controller1)
        let controller2 : MisBeneficiosPorConvenioViewController = MisBeneficiosPorConvenioViewController ()
        controller2.title = "por convenio"
        controllerArray.append(controller2)
        let controller3 : CercanosMapaViewController = CercanosMapaViewController ()
        controller3.title = "cercanos"
        if(BaseMisBeneficiosViewController.goTo == 2)
        {
            controller3.detalleOferta = VariablesGlobales.ofertaMapa
        }
        controllerArray.append(controller3)
 
      
        
        // Customize menu (Optional)
        var parameters: [CAPSPageMenuOption] = [
            .ScrollMenuBackgroundColor(UIColor.whiteColor()),
            .ViewBackgroundColor(UIColor.clearColor()),
            .SelectionIndicatorColor(COLOR_RED),
            .BottomMenuHairlineColor(UIColor.clearColor()),
            .MenuItemFont(UIFont(name: "Signika-Light", size: 13.0)!),
            .MenuHeight(30.0),
            .CenterMenuItems(true)
        ]
        
        if(self.view.frame.size.width <= 375){
            parameters.append(CAPSPageMenuOption.MenuItemWidth(90.0))
        }
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, viewPageMenu.frame.width, viewPageMenu.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        pageMenu?.selectedMenuItemLabelColor = COLOR_RED
        pageMenu?.moveToPage(1)
        pageMenu?.moveToPage(BaseMisBeneficiosViewController.goTo)
        
        if(BaseMisBeneficiosViewController.goTo == 2)
        {
            btEditar.hidden = true
        }
        
        BaseMisBeneficiosViewController.goTo = 0

        
        self.addChildViewController(pageMenu!)
        viewPageMenu.addSubview(pageMenu!.view)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        // MARK: - Scroll menu setup
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if(BaseMisBeneficiosViewController.goTo == 2)
        {
            btEditar.hidden = true
        }
        
        BaseMisBeneficiosViewController.goTo = 0

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editar(sender: AnyObject) {
        
        if(actualPage == 0)
        {
            performSegueWithIdentifier("intereses", sender: nil)
        }
        else if(actualPage == 1)
        {
            performSegueWithIdentifier("seleccion", sender: nil)
        }

    }
    
    func didMoveToPage(controller: UIViewController, index: Int) {
        if(index == 0 || index == 1)
        {
            btEditar.hidden = false
        }
        else
        {
            btEditar.hidden = true
        }
        
        actualPage = index
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
